#pragma once
#include "Enemy.h"

class Viper : public Enemy
{
 public:
    Viper(ne::Vector2f);
    Viper(ne::Vector2f, bool);
    static void init();
    void load();
    void knockBack(int direction);

 private:
    static ne::Image image_;
};

