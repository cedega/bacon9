#pragma once
#include <Neru/Renderer.h>
#include <Neru/Image.h>

class UI
{
    public:
        UI();
        void load();
        void draw(ne::Renderer *renderer);
        
        void setHealth(float percent);
        void updateBattery(int curr, int max);
        
    private:
        ne::Image emptybar_;
        ne::Image health_;
        ne::Image battery_;
        
        int initial_width_;
};          
