#pragma once
#include "Interactable.h"

class Ammo : public Interactable
{
 public:
    Ammo(ID, float, float, float, float);
    static void init();
    void load();
    void onPickup(Player*);
    void draw(ne::Renderer*);
    ne::Rectf getHitbox();

 private:
    static ne::Image ammo_image_;
    ne::Image image_;
};
