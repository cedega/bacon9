#pragma once
#include "Level.h"

class Level2 : public Level
{
 public:
    Level2()
    {

    }

    void setCollisions()
    {
        collisions_.push_back(ne::Rectf(1854,0,554,49));
        collisions_.push_back(ne::Rectf(2701,0,128,49));
        collisions_.push_back(ne::Rectf(2801,46,421,153));
        collisions_.push_back(ne::Rectf(3151,193,91,143));
        collisions_.push_back(ne::Rectf(2301,301,1408,98));
        collisions_.push_back(ne::Rectf(2301,398,948,253));
        collisions_.push_back(ne::Rectf(2851,601,598,148));
        collisions_.push_back(ne::Rectf(2347,651,516,48));
        collisions_.push_back(ne::Rectf(3651,397,77,225));
        collisions_.push_back(ne::Rectf(3601,601,100,293));
        collisions_.push_back(ne::Rectf(3201,851,666,448));
        collisions_.push_back(ne::Rectf(2801,851,248,448));
        collisions_.push_back(ne::Rectf(2351,851,454,148));
        collisions_.push_back(ne::Rectf(2351,998,98,501));
        collisions_.push_back(ne::Rectf(2351,1401,1298,98));
        collisions_.push_back(ne::Rectf(2801,1498,248,51));
        collisions_.push_back(ne::Rectf(3401,1651,248,248));
        collisions_.push_back(ne::Rectf(2801,1651,498,248));
        collisions_.push_back(ne::Rectf(2451,1751,354,98));
        collisions_.push_back(ne::Rectf(2451,1811,148,838));
        collisions_.push_back(ne::Rectf(3751,1297,146,953));
        collisions_.push_back(ne::Rectf(2801,2151,1127,148));
        collisions_.push_back(ne::Rectf(3151,2101,600,51));
        collisions_.push_back(ne::Rectf(3851,2298,91,593));
        collisions_.push_back(ne::Rectf(2901,2298,98,301));
        collisions_.push_back(ne::Rectf(2996,2451,303,148));
        collisions_.push_back(ne::Rectf(3201,2594,98,155));
        collisions_.push_back(ne::Rectf(3451,2451,401,198));
        collisions_.push_back(ne::Rectf(3451,2642,98,107));
        collisions_.push_back(ne::Rectf(3751,2851,248,998));
        collisions_.push_back(ne::Rectf(3951,3848,49,196));
        collisions_.push_back(ne::Rectf(3601,4001,398,803));
        collisions_.push_back(ne::Rectf(2851,4501,348,498));
        collisions_.push_back(ne::Rectf(3194,4651,411,88));
        collisions_.push_back(ne::Rectf(3501,2850,148,399));
        collisions_.push_back(ne::Rectf(3201,2851,300,201));
        collisions_.push_back(ne::Rectf(3299,3052,202,97));
        collisions_.push_back(ne::Rectf(2851,3401,98,998));
        collisions_.push_back(ne::Rectf(2935,3801,264,598));
        collisions_.push_back(ne::Rectf(3351,3801,148,248));
        collisions_.push_back(ne::Rectf(3401,4044,98,155));
        collisions_.push_back(ne::Rectf(3501,3351,98,548));
        collisions_.push_back(ne::Rectf(3599,3351,50,200));
        collisions_.push_back(ne::Rectf(1774,49,125,290));
        collisions_.push_back(ne::Rectf(451,59,1171,140));
        collisions_.push_back(ne::Rectf(1551,198,349,501));
        collisions_.push_back(ne::Rectf(1894,301,155,350));
        collisions_.push_back(ne::Rectf(1900,650,99,948));
        collisions_.push_back(ne::Rectf(1351,851,562,148));
        collisions_.push_back(ne::Rectf(1351,997,298,202));
        collisions_.push_back(ne::Rectf(1001,501,398,198));
        collisions_.push_back(ne::Rectf(901,601,252,148));
        collisions_.push_back(ne::Rectf(1101,646,98,904));
        collisions_.push_back(ne::Rectf(801,851,300,348));
        collisions_.push_back(ne::Rectf(14,59,438,90));
        collisions_.push_back(ne::Rectf(3,149,46,2275));
        collisions_.push_back(ne::Rectf(48,501,601,198));
        collisions_.push_back(ne::Rectf(351,601,398,148));
        collisions_.push_back(ne::Rectf(401,351,98,152));
        collisions_.push_back(ne::Rectf(49,451,150,57));
        collisions_.push_back(ne::Rectf(351,851,348,348));
        collisions_.push_back(ne::Rectf(951,1351,158,198));
        collisions_.push_back(ne::Rectf(1001,1198,106,153));
        collisions_.push_back(ne::Rectf(351,1301,348,198));
        collisions_.push_back(ne::Rectf(551,1351,248,399));
        collisions_.push_back(ne::Rectf(30,1650,370,350));
        collisions_.push_back(ne::Rectf(381,1851,418,148));
        collisions_.push_back(ne::Rectf(1502,1402,399,243));
        collisions_.push_back(ne::Rectf(1351,1301,298,448));
        collisions_.push_back(ne::Rectf(1649,1645,202,55));
        collisions_.push_back(ne::Rectf(951,1651,437,98));
        collisions_.push_back(ne::Rectf(951,1851,648,98));
        collisions_.push_back(ne::Rectf(951,1949,100,50));
        collisions_.push_back(ne::Rectf(1500,1934,99,615));
        collisions_.push_back(ne::Rectf(1201,2451,318,98));
        collisions_.push_back(ne::Rectf(1901,1851,98,898));
        collisions_.push_back(ne::Rectf(1851,2201,61,548));
        collisions_.push_back(ne::Rectf(1201,2651,713,98));
        collisions_.push_back(ne::Rectf(801,2751,848,148));
        collisions_.push_back(ne::Rectf(1100,2898,349,151));
        collisions_.push_back(ne::Rectf(251,2901,977,98));
        collisions_.push_back(ne::Rectf(251,2997,298,102));
        collisions_.push_back(ne::Rectf(532,2981,67,71));
        collisions_.push_back(ne::Rectf(274,2451,725,98));
        collisions_.push_back(ne::Rectf(9,2351,490,114));
        collisions_.push_back(ne::Rectf(61,2540,238,259));
        collisions_.push_back(ne::Rectf(52,2795,97,774));
        collisions_.push_back(ne::Rectf(145,3501,204,1322));
        collisions_.push_back(ne::Rectf(265,4751,2134,248));
        collisions_.push_back(ne::Rectf(2301,4351,98,298));
        collisions_.push_back(ne::Rectf(2151,4351,190,248));
        collisions_.push_back(ne::Rectf(1151,4501,1043,98));
        collisions_.push_back(ne::Rectf(1151,4588,98,61));
        collisions_.push_back(ne::Rectf(2051,3401,348,848));
        collisions_.push_back(ne::Rectf(1151,3751,901,499));
        collisions_.push_back(ne::Rectf(1151,4248,698,151));
        collisions_.push_back(ne::Rectf(1301,3402,549,97));
        collisions_.push_back(ne::Rectf(1351,3474,147,175));
        collisions_.push_back(ne::Rectf(1302,3151,297,289));
        collisions_.push_back(ne::Rectf(951,3151,393,148));
        collisions_.push_back(ne::Rectf(501,3501,298,648));
        collisions_.push_back(ne::Rectf(786,4050,63,101));
        collisions_.push_back(ne::Rectf(796,3750,203,349));
        collisions_.push_back(ne::Rectf(701,3151,98,403));
        collisions_.push_back(ne::Rectf(3249,551,101,50));
        collisions_.push_back(ne::Rectf(49,1601,51,49));
        collisions_.push_back(ne::Rectf(400,1751,50,100));
        collisions_.push_back(ne::Rectf(1150,2651,51,99));
        collisions_.push_back(ne::Rectf(1100,2701,50,50));
        collisions_.push_back(ne::Rectf(3000,2101,152,50));
        collisions_.push_back(ne::Rectf(3050,2051,100,50));
        collisions_.push_back(ne::Rectf(2300,3251,50,150));
        collisions_.push_back(ne::Rectf(2250,3301,50,100));
        collisions_.push_back(ne::Rectf(2150,3351,50,50));
        collisions_.push_back(ne::Rectf(349,4401,51,200));
        collisions_.push_back(ne::Rectf(400,4501,50,100));
        collisions_.push_back(ne::Rectf(450,4551,50,50));
        collisions_.push_back(ne::Rectf(3550,4451,51,200));
        collisions_.push_back(ne::Rectf(3500,4551,50,100));
        collisions_.push_back(ne::Rectf(3450,3701,51,99));
        collisions_.push_back(ne::Rectf(3400,3751,50,50));
        collisions_.push_back(ne::Rectf(2949,3551,51,250));
        collisions_.push_back(ne::Rectf(3000,3701,50,100));
        collisions_.push_back(ne::Rectf(3100,3751,50,50));
        collisions_.push_back(ne::Rectf(1999,651,24,27));
        collisions_.push_back(ne::Rectf(2322,651,25,24));
        collisions_.push_back(ne::Rectf(3249,3051,50,49));
        collisions_.push_back(ne::Rectf(3599,3551,26,27));
        collisions_.push_back(ne::Rectf(1649,999,27,27));
        collisions_.push_back(ne::Rectf(1051,1949,28,25));
        collisions_.push_back(ne::Rectf(521,1498,30,28));
        collisions_.push_back(ne::Rectf(1849,4250,28,28));
        collisions_.push_back(ne::Rectf(549,3052,26,26));
        collisions_.push_back(ne::Rectf(1649,2749,57,51));

        platforms_.push_back(ne::Rectf(2049,300,252,5));
        platforms_.push_back(ne::Rectf(3449,701,152,5));
        platforms_.push_back(ne::Rectf(3049,851,152,5));
        platforms_.push_back(ne::Rectf(3649,1651,102,5));
        platforms_.push_back(ne::Rectf(3299,1651,102,5));
        platforms_.push_back(ne::Rectf(2599,2151,202,5));
        platforms_.push_back(ne::Rectf(2400,1750,51,5));
        platforms_.push_back(ne::Rectf(1999,1851,51,5));
        platforms_.push_back(ne::Rectf(1599,1851,302,5));
        platforms_.push_back(ne::Rectf(1649,1150,151,5));
        platforms_.push_back(ne::Rectf(499,351,1052,5));
        platforms_.push_back(ne::Rectf(749,701,152,5));
        platforms_.push_back(ne::Rectf(699,851,102,5));
        platforms_.push_back(ne::Rectf(49,1001,302,5));
        platforms_.push_back(ne::Rectf(49,1150,101,5));
        platforms_.push_back(ne::Rectf(49,1301,302,5));
        platforms_.push_back(ne::Rectf(49,1451,302,5));
        platforms_.push_back(ne::Rectf(799,1350,152,5));
        platforms_.push_back(ne::Rectf(1199,851,152,5));
        platforms_.push_back(ne::Rectf(1199,1301,152,5));
        platforms_.push_back(ne::Rectf(799,1951,152,5));
        platforms_.push_back(ne::Rectf(750,2101,300,5));
        platforms_.push_back(ne::Rectf(500,2251,250,5));
        platforms_.push_back(ne::Rectf(1150,2251,350,5));
        platforms_.push_back(ne::Rectf(1000,2500,200,5));
        platforms_.push_back(ne::Rectf(1599,2001,302,5));
        platforms_.push_back(ne::Rectf(149,2901,102,5));
        platforms_.push_back(ne::Rectf(149,3051,102,5));
        platforms_.push_back(ne::Rectf(149,3200,101,5));
        platforms_.push_back(ne::Rectf(149,3350,101,5));
        platforms_.push_back(ne::Rectf(799,3301,151,5));
        platforms_.push_back(ne::Rectf(799,3450,502,5));
        platforms_.push_back(ne::Rectf(799,3601,552,5));
        platforms_.push_back(ne::Rectf(999,3901,152,5));
        platforms_.push_back(ne::Rectf(999,4051,152,5));
        platforms_.push_back(ne::Rectf(900,4201,251,5));
        platforms_.push_back(ne::Rectf(349,4601,802,5));
        platforms_.push_back(ne::Rectf(349,3501,152,5));
        platforms_.push_back(ne::Rectf(349,3651,152,5));
        platforms_.push_back(ne::Rectf(349,3801,152,5));
        platforms_.push_back(ne::Rectf(349,3951,152,5));
        platforms_.push_back(ne::Rectf(349,4101,152,5));
        platforms_.push_back(ne::Rectf(349,4251,201,5));
        platforms_.push_back(ne::Rectf(1499,3601,552,5));
        platforms_.push_back(ne::Rectf(1850,3451,201,5));
        platforms_.push_back(ne::Rectf(1599,3151,201,5));
        platforms_.push_back(ne::Rectf(3199,3951,152,5));
        platforms_.push_back(ne::Rectf(3199,3801,152,5));
        platforms_.push_back(ne::Rectf(3649,3351,102,5));
        platforms_.push_back(ne::Rectf(3450,3350,51,5));
        platforms_.push_back(ne::Rectf(2399,3401,452,5));
        platforms_.push_back(ne::Rectf(2949,3401,151,5));
        platforms_.push_back(ne::Rectf(2450,3151,200,5));
        platforms_.push_back(ne::Rectf(2650,3001,551,5));
        platforms_.push_back(ne::Rectf(2550,2851,651,5));
        platforms_.push_back(ne::Rectf(3649,3001,102,5));
        platforms_.push_back(ne::Rectf(3649,2851,102,5));
        platforms_.push_back(ne::Rectf(3299,2701,152,5));
        platforms_.push_back(ne::Rectf(3299,2551,152,5));
        platforms_.push_back(ne::Rectf(2399,4751,452,5));
        platforms_.push_back(ne::Rectf(999,3751,152,5));
    }

    void load()
    {
        tile_map_.loadFromFile("data/level2.tmx", "data/finish.png");
        setCollisions();

        // Collisions to prevent falling off the map
        collisions_.push_back(ne::Rectf(-200, 0, 200, tile_map_.getPixelSize().y));
        // collisions_.push_back(ne::Rectf(0, -200, tile_map_.getPixelSize().x, 200));
        collisions_.push_back(ne::Rectf(0, tile_map_.getPixelSize().y, tile_map_.getPixelSize().x, 200));
        collisions_.push_back(ne::Rectf(tile_map_.getPixelSize().x, 0, 200, tile_map_.getPixelSize().y));

        collisions_.push_back(ne::Rectf(2700, -800, 1290, 850));
        collisions_.push_back(ne::Rectf(833, -800, 1517, 850));

        player_->setCollisions(&collisions_);
        player_->setPlatforms(&platforms_);
        player_->setPosition(ne::Vector2f(2503, -200));
        player_->getDrone()->setPosition(player_->getCentre());
        player_->getDrone()->getLight()->setPosition(player_->getCentre(), true);
        player_->getDrone()->input(90.0f);

        player_->setBatteryThres(1.0);

        camera_->setLevelSize(tile_map_.getPixelSize());
        camera_->setPosition(player_->getCentre());

        // Default Enemies
        
        enemies_.push_back(new Viper(ne::Vector2f(3059, 216)));
        enemies_.push_back(new Blob(ne::Vector2f(2672, 765)));
        enemies_.push_back(new Viper(ne::Vector2f(59, 1204)));
        enemies_.push_back(new Viper(ne::Vector2f(3496, 1583)));
        enemies_.push_back(new Viper(ne::Vector2f(3723, 2753)));
        enemies_.push_back(new Blob(ne::Vector2f(3049, 2766)));
        enemies_.push_back(new Merman(ne::Vector2f(2583, 1668)));
        enemies_.push_back(new Viper(ne::Vector2f(1688, 3293)));
        enemies_.push_back(new Blob(ne::Vector2f(1273, 4649)));
        enemies_.push_back(new Viper(ne::Vector2f(1143, 248)));
        enemies_.push_back(new Viper(ne::Vector2f(123, 2176)));
        enemies_.push_back(new Viper(ne::Vector2f(163, 2176)));
        enemies_.push_back(new Blob(ne::Vector2f(1243, 1757)));
        enemies_.push_back(new Blob(ne::Vector2f(585, 2774)));
        enemies_.push_back(new Merman(ne::Vector2f(3033, 3541)));

        for (unsigned int i = 0; i < enemies_.size(); i++)
        {
            enemies_[i]->setCollisions(&collisions_);
            enemies_[i]->setPlatforms(&platforms_);
        }

        // Spawn Enemies!
    
        interactables_.push_back(new Spawner(SPAWNER, 198, 149, 205, 353, MERMAN_NOALERT, ne::Vector2f(1505, 275))); 
        interactables_.push_back(new Spawner(SPAWNER, 1676, 1031, 133, 137, VIPER, ne::Vector2f(1219, 1207))); 
        interactables_.push_back(new Spawner(SPAWNER, 1400, 2550, 100, 100, VIPER_NOALERT, ne::Vector2f(1908, 1758))); 
        interactables_.push_back(new Spawner(SPAWNER, 349, 4600, 167, 151, MERMAN_NOALERT, ne::Vector2f(383, 3406))); 
        interactables_.push_back(new Spawner(SPAWNER, 3197, 4502, 203, 151, VIPER_NOALERT, ne::Vector2f(3245, 3714))); 
        interactables_.push_back(new Spawner(SPAWNER, 3250, 398, 197, 151, VIPER, ne::Vector2f(3069, 765))); 
        interactables_.push_back(new Spawner(SPAWNER, 2447, 999, 159, 402, VIPER, ne::Vector2f(3450, 1316))); 
        interactables_.push_back(new Spawner(SPAWNER, 2447, 998, 159, 402, MERMAN, ne::Vector2f(3560, 1316))); 

        // Upgrades!

        interactables_.push_back(new Upgrade(UPGRADE, 3270, 475, 32, 32, HEALTH));
        interactables_.push_back(new Upgrade(UPGRADE, 3228, 4555, 32, 32, HEALTH));
        interactables_.push_back(new Upgrade(UPGRADE, 2473, 1278, 32, 32, DAMAGE));
        interactables_.push_back(new Upgrade(UPGRADE, 77, 359, 32, 32, DAMAGE));
        interactables_.push_back(new Upgrade(UPGRADE, 1810, 767, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 721, 3061, 32, 32, HEALTH));
        interactables_.push_back(new Upgrade(UPGRADE, 403, 4682, 32, 32, DAMAGE));

        // Dialog!
        interactables_.push_back(new Location(LOCATION, "Is that a harpoon gun over there?", 2502, 244, 153, 56));
        interactables_.push_back(new Location(LOCATION, "Large life-form detected below.", 2400, 4550, 450, 215));
        interactables_.push_back(new Location(LOCATION, "Only one way to go...", 2123, 242, 100, 55));
        interactables_.push_back(new Location(LOCATION, "Wheee", 1992, 1173, 363, 132));
        interactables_.push_back(new Location(LOCATION, "I wonder what they keep in those boxes?", 2053, 3215, 345, 183));
        interactables_.push_back(new Location(LOCATION, "Have you found some harpoon ammo?", 1439, 3649, 49, 100));
        interactables_.push_back(new Location(LOCATION, "Are you lost?", 500, 4496, 646, 106));
        interactables_.push_back(new Location(LOCATION, "Do you feel like sushi?", 348, 3440, 155, 60));
        interactables_.push_back(new Location(LOCATION, "I think you should be going down.", 404, 294, 96, 57));
        interactables_.push_back(new Location(LOCATION, "They used to do world class research here.", 1400, 546, 152, 81));
        interactables_.push_back(new Location(LOCATION, "Marine experts from all over gathered here.", 1200, 973, 150, 68));
        interactables_.push_back(new Location(LOCATION, "Now look at this place...", 1200, 1503, 149, 46));
        interactables_.push_back(new Location(LOCATION, "The spill changed everything.", 952, 1549, 28, 100));
        interactables_.push_back(new Location(LOCATION, "Nice moves!", 2352, 698, 50, 153));
        interactables_.push_back(new Location(LOCATION, "Watch your step.", 3049, 1149, 154, 57));
        interactables_.push_back(new Location(LOCATION, "Shutting down in 3, 2, 1", 3655, 1329, 67, 74));
        interactables_.push_back(new Location(LOCATION, "Just kidding!", 3656, 1482, 95, 18));
        interactables_.push_back(new Location(LOCATION, "Nice pipes!", 3650, 3148, 101, 53));
        interactables_.push_back(new Location(LOCATION, "I feel like I'm getting stronger.", 3852, 2895, 52, 103));
        
        // Ammo!

        interactables_.push_back(new Weapon(HARPOON, 1960, 245, 32, 32));

        interactables_.push_back(new Ammo(AMMO, 3858, 3960, 32, 32));
        interactables_.push_back(new Ammo(AMMO, 2058, 4459, 32, 32));
        interactables_.push_back(new Ammo(AMMO, 1459, 2209, 32, 32));

        interactables_.push_back(new Upgrade(UPGRADE, 3118, 2376, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 2538, 3100, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 2600, 4679, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 549, 2373, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 1000, 280, 32, 32, BATTERY));

        // Level!
        interactables_.push_back(new Transition(LEVEL3, 2400, 4950, 450, 100));
    }
  
 private:
};

