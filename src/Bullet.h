#pragma once
#include <Neru/Entity.h>

class Bullet : public ne::Entity
{
 public:
    Bullet(const ne::Vector2f &position, const ne::Vector2f &direction, float life_time, int damage);
    virtual ~Bullet() {}
    void load();
    void update(const float dt);
    void transitionAnimations() {}

    bool isAlive() const { return alive_; }
    int getDamage() const { return damage_; }

 private:
    ne::EntityData data_;
    float life_time_;
    float life_tick_;
    bool alive_;
    int damage_;
};
