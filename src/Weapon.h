#pragma once
#include "Interactable.h"

class Weapon : public Interactable
{
 public:
    Weapon(ID, float, float, float, float);
    static void init();
    void load();
    void onPickup(Player*);
    void draw(ne::Renderer*);
    ne::Rectf getHitbox();

 private:
    static ne::Image knife_image_;
    static ne::Image harpoon_image_;
    ne::Image image_;
};
