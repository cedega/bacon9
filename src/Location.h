#pragma once
#include "Interactable.h"

class Location : public Interactable
{
    public:
        Location(ID, const std::string&, float, float, float, float);
        void load() {};
        void onPickup(Player* player);
        ne::Rectf getHitbox();
    private:
        ne::Text text_;
};

