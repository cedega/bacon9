#pragma once
#include <Neru/Engine.h>
#include "Map.h"
#include "TitleScreen.h"

class Engine : public ne::Engine
{
 public:
    Engine();
    void events(const SDL_Event &event);
    void logic(const float dt);
    void render(ne::Renderer *renderer);

 private:
    Map map_;
    TitleScreen title_screen_;
};

