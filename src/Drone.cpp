#include "Drone.h"

Drone::Drone()
{
    rotation_ = 0.0f;
    text_timer_ = 0.0f;
    flicker_ = false;
    flicker_count_ = 0;
}

void Drone::load()
{
    setHitbox(ne::Vector2f(1, 1));

    data_.loadFileImage("data/drone.png");
    data_.setSpriteSheet(64, 64, 0);
    data_.setBaseDisplacement(-32, -32, 1);

    data_.newAnimation("idle", 1, 1, 100);

    ne::EntityImage::attach(&data_);
    loadAnimation("idle");

    setGravity(0);
    setSpeed(0.0f);
    text_.loadFileFont("data/font.ttf");
    text_.setString("We should find a weapon.");
    text_timer_ = 0;

    // setScaling(ne::Vector2f(2.0f, 2.0f));

    white_.loadFileImage("data/light2.png");
    black_.loadFileImage("data/black.png");

    light_.setTexture(&white_);
}

void Drone::reset()
{
    setSpeed(0.0f);
    text_.setString("We should find a weapon.");
    text_timer_ = 0;
    loadAnimation("idle");
    rotation_ = 0.0f;
    flicker_ = false;
    flicker_count_ = 0;

    light_.setTexture(&white_);
}

void Drone::input(float rotation)
{
    light_.setRotation(rotation);
    ne::EntityImage::setFlip(!(rotation >= -90.0f && rotation <= 90.0f));
}

void Drone::AI(ne::Entity *target, int battery, const float dt)
{
    if (battery < 0) battery = 0;

    ne::Vector2f offset = target->getDirection() < 0 ? ne::Vector2f(target->getHitbox().w, -50) : ne::Vector2f(-target->getHitbox().w, -50);
    ne::Vector2f difference = (target->getCentre() + offset) - getCentre();

    if (battery <= 5)
    {
        if (!flicker_)
        {
            if (battery == 0)
            {
                flicker_ = true;
                flicker_count_ = 10;
            }
            else
            {
                flicker_ = ne::random(1, battery * battery * battery) == 1;
                if (flicker_)
                    flicker_count_ = 10;
            }
        }

        if (flicker_count_ > 0)
        {
            light_.setTexture(&black_);
            flicker_count_--;
        }
        else if (battery > 0)
        {
            light_.setTexture(&white_);
            flicker_ = false;
        }
    }

    float speed = difference.length() * 1.5f;
    if (speed < 5.0f)
        speed = 0.0f;

    setSpeed(speed);

    if (text_timer_ < 5.0)
    {
        text_timer_ += dt;
        text_.setPosition(ne::Vector2f(getPosition().x - (text_.getW() / 2.0f) + 16, getPosition().y - (text_.getH() / 2.0f) - 32));
    }
    else
    {
        text_.setString("");
    }

    ne::Entity::input(difference, false, false);
    ne::Entity::logic(dt);
    ne::Entity::update(dt);

    light_.setPosition(getCentre(), true);
    text_.updateLast();
}

void Drone::drawLight(ne::Renderer *renderer)
{
    renderer->draw(light_);
}

ne::Image* Drone::getLight()
{
    return &light_;
}

void Drone::useLight()
{
    flicker_ = false;
    flicker_count_ = 0;
    light_.setTexture(&white_);
}

void Drone::setText(const std::string& text)
{
    text_.setString(text);
    text_timer_ = 0;
}

ne::Text& Drone::getText()
{
    return text_;
}

