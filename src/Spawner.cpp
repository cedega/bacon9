#include "Spawner.h"

Spawner::Spawner(ID id, float x, float y, float width, float height, SPAWN_TYPE type, ne::Vector2f pos)
{
    id_ = id;
    hitbox_ = ne::Rectf(x, y, width, height);
    active_ = true;
    pos_ = pos;
    type_ = type;
}

void Spawner::onPickup(Player *player)
{
    active_ = false;
}

Enemy* Spawner::getEnemy()
{
    switch (type_)
    {
    case VIPER: return new Viper(pos_); break;
    case MERMAN: return new Merman(pos_); break;
    case VIPER_NOALERT: return new Viper(pos_, false); break;
    case MERMAN_NOALERT: return new Merman(pos_, false); break;
    }

    return NULL;
} 

ne::Vector2f Spawner::getPos()
{
    return pos_;
}

