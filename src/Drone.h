#pragma once
#include <Neru/Entity.h>
#include <Neru/Renderer.h>
#include <cmath>

class Drone : public ne::Entity
{
 public:
    Drone();
    void load();
    void reset();
    void input(float rotation);
    void AI(ne::Entity *target, int battery, const float dt);
    void drawLight(ne::Renderer *renderer);
    void transitionAnimations() {} 
    void setText(const std::string&);
    ne::Text& getText();
    void useLight();

    ne::Image* getLight();

 private:
    ne::EntityData data_; 
    ne::Image light_;
    ne::Image black_;
    ne::Image white_;
    float rotation_;
    ne::Text text_;
    float text_timer_;
    bool flicker_;
    int flicker_count_;
};
