#pragma once
#include "Interactable.h"
#include <Neru/Image.h>

class Upgrade : public Interactable
{
 public:
    Upgrade(ID, float, float, float, float, UPGRADE_TYPE);
    static void init();
    void load();
    void onPickup(Player*);
    void draw(ne::Renderer *renderer);

 private:
    UPGRADE_TYPE type_;
    static ne::Image health_image_;
    static ne::Image damage_image_;
    static ne::Image battery_image_;
    ne::Image image_;
};

