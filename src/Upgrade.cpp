#include "Upgrade.h"

ne::Image Upgrade::health_image_;
ne::Image Upgrade::damage_image_;
ne::Image Upgrade::battery_image_;

Upgrade::Upgrade(ID id, float x, float y, float width, float height, UPGRADE_TYPE type)
{
    id_ = id;
    hitbox_ = ne::Rectf(x, y, width, height);
    active_ = true;
    type_ = type;

    load();
}

void Upgrade::init()
{
    health_image_.loadFileImage("data/healthupgrade.png");
    damage_image_.loadFileImage("data/damageupgrade.png");
    battery_image_.loadFileImage("data/batteryupgrade.png");
}

void Upgrade::load()
{
    switch (type_)
    {
    case HEALTH: image_.setTexture(&health_image_); info_.setString("HEALTH UP"); break;
    case BATTERY: image_.setTexture(&battery_image_); info_.setString("BATTERY UP"); break;
    case DAMAGE: image_.setTexture(&damage_image_); info_.setString("DAMAGE UP"); break;
    }

    ne::Vector2f centre = ne::Vector2f(hitbox_.x + hitbox_.w / 2.0f, hitbox_.y - 12.0f);
    info_.setPosition(ne::Vector2f(centre.x - info_.getW() / 2.0f, centre.y - info_.getH() / 2.0f));
    image_.setPosition(ne::Vector2f(hitbox_.x, hitbox_.y));
    
    pick_up_sound_.loadFileWAV("data/pick_up_item.wav");
}

void Upgrade::onPickup(Player *player)
{
    pick_up_sound_.play();
    active_ = false;
    player->getUpgrade(type_);
}

void Upgrade::draw(ne::Renderer *renderer)
{
    if (active_)
        renderer->draw(image_);
    Interactable::draw(renderer);
}

