#include "Blob.h"

ne::Image Blob::image_;

Blob::Blob(ne::Vector2f pos)
{
    load();
    respects_alert_ = true;
    setPosition(pos);
}

Blob::Blob(ne::Vector2f pos, bool alert)
{
    load();
    respects_alert_ = alert;
    setPosition(pos);
}

void Blob::init()
{
    image_.loadFileImage("data/blobfishspritesheet.png");
}

void Blob::load()
{
    // common properties
    health_ = 24;
    damage_ = 25.0f;
    max_speed_ = 100.0f;
    within_attack_distance_ = false;
    alert_distance_ = 400;
    setSpeed(25.0f);
    setJump(900.0f);
    //  load images and shit here
    setHitbox(ne::Vector2f(80, 55));

    // Loads the spritesheet with "movement" animations
    data_.setTexture(&image_); 
    data_.setSpriteSheet(150, 150, 2);
    data_.setBaseDisplacement(-40, -95, 80);
                                            
    // ------------------------------------------------
    // Initialize "movement" animations
    // ------------------------------------------------
    data_.newAnimation("idle", 1, 5, 100);
        data_.setLoop(true);
    data_.newAnimation("walk", 6, 11, 50);
        data_.setLoop(true);
    data_.newAnimation("attack", 12, 27, 50);
        data_.setActiveFrames(19, 22);
        data_.setActiveRegion(19, ne::Rectf(91, 84, 41, 42));
        data_.setActiveRegion(20, ne::Rectf(91, 84, 41, 42));
        data_.setActiveRegion(21, ne::Rectf(91, 84, 41, 42));
        data_.setLoop(false);
    data_.newAnimation("death", 28, 36, 50);
        data_.setLoop(false);

    // Bind both pieces of data to the image handling portion of ne::Entity (ne::EntityImage)
    ne::EntityImage::attach(&data_);

    // Initalially load the idle animation, and set the speed and jump height of the entity.
    loadAnimation("idle");
    
    // Load Sounds
    killed_sound_.loadFileWAV("data/new_death.wav");
    hit_sound_.loadFileWAV("data/enemy_hit.wav");
}

void Blob::knockBack(int direction)
{
    if (!forceExistsX(knockback_id_))
    {
        knockback_id_ = addForceX(200 * -direction, 0, 150); 
        setSpeed(0); 
    }
}

void Blob::transitionAnimations()
{
    if (!alive_)
    {
        loadAnimation("death");
    }
    else if (isAnimating("death"))
    {
        // Prevent state transition
    }
    else if (within_attack_distance_ && !isAnimating("attack"))
    {
        loadAnimation("attack");
        setSpeed(0.0f);
        setJump(0.0f);
    }
    else if (isAnimating("attack"))
    {
        if (animationStopped())
        {
            loadAnimation("idle");
        }
        setSpeed(0.0f);
    }

    else if (!isMoving() && !isAnimating("idle") && !isAnimating("attack"))
    { 
        loadAnimation("idle");
        setSpeed(25.0f);
        setJump(900.0f);
    }
    else if (!isAnimating("walk") && isMoving() && !isAnimating("attack"))
    {
        loadAnimation("walk");
        setSpeed(25.0f);
        setJump(900.0f);
    }
}

void Blob::AI(const Player *player, const float dt) 
{
    if (!alive_)
    {
        ne::Entity::input(ne::Vector2f(0, 0), false, false);
        return;
    }

    // visibility testing
    bool visible = true;
    if (getCollisions() != NULL)
    {
        for (size_t i = 0; i < getCollisions()->size(); i++)
        {
            if (ne::intersects(player->getPosition(), getPosition(), getCollisions()->at(i), NULL) )
            {
                visible = false;
            }
        }
    }

    ne::Vector2f dist_from_player = player->getPosition() - getPosition();
    // within range of player
    if (( (dist_from_player.ne::Vector2f::length() < alert_distance_ && respects_alert_) || !respects_alert_) &&
            (visible || dist_from_player.ne::Vector2f::length() < critical_distance_ || !respects_alert_))
    {
        within_attack_distance_ = (ne::absval(dist_from_player.x) < 20);
        aggro_timer_ += dt / 2;
        setSpeed(std::min(getSpeed() + aggro_timer_, max_speed_));
        ne::Vector2f direction;
        if (ne::absval(dist_from_player.x) > 50)
        {
            direction.x = dist_from_player.x;
            prev_dir_ = direction.x;
        }
        else
        {
            direction.x = prev_dir_;
        }
        if (ne::absval(direction.x) < 1)
        {
            direction.x = 0;
        }
        // player above enemy
        if (player->getPosition().y < getPosition().y &&
                // x position where jumping starts
                ne::absval(dist_from_player.x) < X_JUMP_DISTANCE &&
                // y position difference not less than some arbitrary small value to prevent constant jumping
                ne::absval(dist_from_player.y) > 75 )
        {
                ne::Entity::input(direction, true, true);
        }
        // player below enemy
        else if (player->getPosition().y > getPosition().y &&
                ne::absval(dist_from_player.y) > 10)
        {
            fallThroughPlatform(true);
            ne::Entity::input(direction, false, true);
        }
        else if (player->getPosition().y < getPosition().y)
        {
            fallThroughPlatform(false);
            ne::Entity::input(direction, false, true);
        }
        else
        {
            ne::Entity::input(direction, false, true);
        } 
    }
    // outside of alert distance
    else
    {
        ne::Entity::input(ne::Vector2f(0, 0), false, false);
        aggro_timer_ = 0;
    }
}
