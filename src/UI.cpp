#include "UI.h"

UI::UI()
{
}

void UI::load()
{
    emptybar_.loadFileImage("data/emptybar.png");
    emptybar_.setX(0.0);
    emptybar_.setY(10.0);
    
    health_.loadFileImage("data/healthbar.png");
    health_.setX(6.0);
    health_.setY(25.0);
    
    battery_.loadFileImage("data/batterybar.png");
    battery_.setX(16.0);
    battery_.setY(14.0);
    
    initial_width_ = health_.getW();
}

void UI::draw(ne::Renderer *renderer)
{
    renderer->draw(emptybar_);
    renderer->draw(health_);
    renderer->draw(battery_);
}

void UI::setHealth(float percent)
{
    ne::Recti temp(0, 0, ne::round(percent * health_.getImageW()), health_.getH());
    health_.setTextureRect(temp);
    /*
    ne::Recti temp(0, 0, health_.getW(), round(percent*initial_height_));
    health_.setTextureRect(temp);
    // 5 is the width of the health bar at the bottom, needed to place the health properly.
    health_.setY(health_bar_.getH() - health_.getH() + 5); 
    */
}

void UI::updateBattery(int curr, int max)
{
    float ratio = curr / (max * 1.0);

    ne::Recti temp(0, 0, ne::round(ratio * battery_.getImageW()), battery_.getH());
    battery_.setTextureRect(temp);
    // 5 is the width of the battery bar at the bottom, needed to place the battery properly.
    // battery_.setY(battery_bar_.getH() - battery_.getH() + 5);
}
