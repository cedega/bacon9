#pragma once
#include "Interactable.h"

class Transition : public Interactable
{
 public:
    Transition(ID, float, float, float, float);
    void load() {};
    void onPickup(Player* player);
    ne::Rectf getHitbox();
 private:
};

