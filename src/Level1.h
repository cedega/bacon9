#pragma once
#include "Level.h"

class Level1 : public Level
{
 public:
    Level1()
    {

    }

    void setCollisions()
    {
        collisions_.push_back(ne::Rectf(1449,2451,102,198));
        collisions_.push_back(ne::Rectf(3299,2650,152,99));
        collisions_.push_back(ne::Rectf(2400,1900,50,101));
        collisions_.push_back(ne::Rectf(3850,2600,51,51));
        collisions_.push_back(ne::Rectf(0,2292,302,57));
        collisions_.push_back(ne::Rectf(351,1851,48,448));
        collisions_.push_back(ne::Rectf(179,1851,220,36));
        collisions_.push_back(ne::Rectf(162,1226,37,625));
        collisions_.push_back(ne::Rectf(0,201,399,898));
        collisions_.push_back(ne::Rectf(2,2451,1447,198));
        collisions_.push_back(ne::Rectf(297,2642,102,324));
        collisions_.push_back(ne::Rectf(386,2951,566,24));
        collisions_.push_back(ne::Rectf(901,2801,648,167));
        collisions_.push_back(ne::Rectf(901,2643,548,56));
        collisions_.push_back(ne::Rectf(601,2101,98,150));
        collisions_.push_back(ne::Rectf(698,2151,851,98));
        collisions_.push_back(ne::Rectf(699,2248,603,101));
        collisions_.push_back(ne::Rectf(1301,2248,48,53));
        collisions_.push_back(ne::Rectf(301,1451,98,298));
        collisions_.push_back(ne::Rectf(601,1801,298,198));
        collisions_.push_back(ne::Rectf(651,1751,748,100));
        collisions_.push_back(ne::Rectf(851,1979,498,70));
        collisions_.push_back(ne::Rectf(1291,1801,258,98));
        collisions_.push_back(ne::Rectf(1284,1301,65,680));
        collisions_.push_back(ne::Rectf(1051,1561,78,220));
        collisions_.push_back(ne::Rectf(301,1451,751,148));
        collisions_.push_back(ne::Rectf(651,1301,638,171));
        collisions_.push_back(ne::Rectf(601,1351,76,123));
        collisions_.push_back(ne::Rectf(551,1401,65,66));
        collisions_.push_back(ne::Rectf(501,201,1598,798));
        collisions_.push_back(ne::Rectf(501,999,1098,100));
        collisions_.push_back(ne::Rectf(751,1099,148,100));
        collisions_.push_back(ne::Rectf(1201,1099,148,100));
        collisions_.push_back(ne::Rectf(1543,2851,2906,448));
        collisions_.push_back(ne::Rectf(1551,2451,1198,105));
        collisions_.push_back(ne::Rectf(1701,2151,448,305));
        collisions_.push_back(ne::Rectf(1551,2556,1198,93));
        collisions_.push_back(ne::Rectf(1651,2651,1648,48));
        collisions_.push_back(ne::Rectf(1699,2699,250,50));
        collisions_.push_back(ne::Rectf(2501,2699,798,50));
        collisions_.push_back(ne::Rectf(1701,1151,348,748));
        collisions_.push_back(ne::Rectf(1651,1451,250,151));
        collisions_.push_back(ne::Rectf(2047,1801,202,98));
        collisions_.push_back(ne::Rectf(2047,1651,52,153));
        collisions_.push_back(ne::Rectf(2038,1151,1011,98));
        collisions_.push_back(ne::Rectf(2040,1244,109,207));
        collisions_.push_back(ne::Rectf(2143,1244,758,105));
        collisions_.push_back(ne::Rectf(2201,201,1598,798));
        collisions_.push_back(ne::Rectf(3001,551,998,498));
        collisions_.push_back(ne::Rectf(3790,201,1210,198));
        collisions_.push_back(ne::Rectf(4151,551,601,498));
        collisions_.push_back(ne::Rectf(4351,398,85,166));
        collisions_.push_back(ne::Rectf(4701,941,148,1108));
        collisions_.push_back(ne::Rectf(4847,1049,153,100));
        collisions_.push_back(ne::Rectf(2451,1601,348,498));
        collisions_.push_back(ne::Rectf(2401,1651,57,98));
        collisions_.push_back(ne::Rectf(2551,2087,148,62));
        collisions_.push_back(ne::Rectf(2901,2451,848,98));
        collisions_.push_back(ne::Rectf(3001,2151,697,148));
        collisions_.push_back(ne::Rectf(3601,2101,248,99));
        collisions_.push_back(ne::Rectf(3599,2297,102,52));
        collisions_.push_back(ne::Rectf(3451,2651,998,98));
        collisions_.push_back(ne::Rectf(4951,1251,49,1164));
        collisions_.push_back(ne::Rectf(4751,2251,249,1049));
        collisions_.push_back(ne::Rectf(3001,1733,98,266));
        collisions_.push_back(ne::Rectf(2901,1601,167,348));
        collisions_.push_back(ne::Rectf(3051,1401,98,260));
        collisions_.push_back(ne::Rectf(3012,1601,715,198));
        collisions_.push_back(ne::Rectf(3601,1797,248,202));
        collisions_.push_back(ne::Rectf(3701,1551,783,348));
        collisions_.push_back(ne::Rectf(3701,1151,898,248));
        collisions_.push_back(ne::Rectf(3651,1151,59,101));
        collisions_.push_back(ne::Rectf(4301,1393,117,167));
        collisions_.push_back(ne::Rectf(4479,1396,120,604));
        collisions_.push_back(ne::Rectf(4151,1895,400,154));
        collisions_.push_back(ne::Rectf(4351,2049,98,611));
        collisions_.push_back(ne::Rectf(3901,2451,498,203));
        collisions_.push_back(ne::Rectf(4151,2201,201,271));
        collisions_.push_back(ne::Rectf(4051,2201,104,98));
        collisions_.push_back(ne::Rectf(399,2901,101,50));
        collisions_.push_back(ne::Rectf(500,2351,50,100));
        collisions_.push_back(ne::Rectf(450,2401,50,50));
        collisions_.push_back(ne::Rectf(1600,2351,101,100));
        collisions_.push_back(ne::Rectf(1650,2301,51,50));
        collisions_.push_back(ne::Rectf(2149,2301,51,150));
        collisions_.push_back(ne::Rectf(2200,2401,100,50));
        collisions_.push_back(ne::Rectf(2750,1101,100,50));
        collisions_.push_back(ne::Rectf(3200,2051,50,100));
        collisions_.push_back(ne::Rectf(3250,2101,50,50));
        collisions_.push_back(ne::Rectf(4750,2201,50,50));
        collisions_.push_back(ne::Rectf(3350,1551,200,50));
        collisions_.push_back(ne::Rectf(3400,1501,100,51));
        collisions_.push_back(ne::Rectf(3450,1451,50,53));
        collisions_.push_back(ne::Rectf(399,2649,26,28));
        collisions_.push_back(ne::Rectf(302,2292,29,30));
        collisions_.push_back(ne::Rectf(331,2278,20,22));
        collisions_.push_back(ne::Rectf(655,2251,44,56));
        collisions_.push_back(ne::Rectf(624,2251,31,24));
        collisions_.push_back(ne::Rectf(676,2307,23,20));
        collisions_.push_back(ne::Rectf(172,1099,79,104));
        collisions_.push_back(ne::Rectf(250,1100,52,50));
        collisions_.push_back(ne::Rectf(1349,1099,27,26));
        collisions_.push_back(ne::Rectf(1672,2699,27,25));
        collisions_.push_back(ne::Rectf(3572,2299,28,24));
        collisions_.push_back(ne::Rectf(3698,2200,80,73));
        collisions_.push_back(ne::Rectf(3698,2273,41,40));
        collisions_.push_back(ne::Rectf(3778,2200,37,36));
        collisions_.push_back(ne::Rectf(4551,2000,25,25));
        collisions_.push_back(ne::Rectf(1349,1899,26,28));
        collisions_.push_back(ne::Rectf(1302,2301,24,24));
        collisions_.push_back(ne::Rectf(2900,1249,55,49));
        collisions_.push_back(ne::Rectf(2049,1451,51,54));
        collisions_.push_back(ne::Rectf(3672,1252,29,24));

        platforms_.push_back(ne::Rectf(100 + 298,2250,52,5));
        platforms_.push_back(ne::Rectf(399,2101,202,5));
        platforms_.push_back(ne::Rectf(399,2000,51,4));
        platforms_.push_back(ne::Rectf(550,1900,51,5));
        platforms_.push_back(ne::Rectf(199,1700,102,6));
        platforms_.push_back(ne::Rectf(199,1550,102,5));
        platforms_.push_back(ne::Rectf(1449,2451,102,5));
        platforms_.push_back(ne::Rectf(1449,2601,102,5));
        platforms_.push_back(ne::Rectf(1529,2150,152,5));
        platforms_.push_back(ne::Rectf(1349,2001,1102,5));
        platforms_.push_back(ne::Rectf(1549,1851,152,5));
        platforms_.push_back(ne::Rectf(1349,1601,352,5));
        platforms_.push_back(ne::Rectf(399,1051,102,5));
        platforms_.push_back(ne::Rectf(1650,1300,51,5));
        platforms_.push_back(ne::Rectf(1650,1150,51,5));
        platforms_.push_back(ne::Rectf(2249,1800,51,5));
        platforms_.push_back(ne::Rectf(2099,1650,151,4));
        platforms_.push_back(ne::Rectf(3299,2650,152,6));
        platforms_.push_back(ne::Rectf(3749,2451,152,5));
        platforms_.push_back(ne::Rectf(3848,2100,52,5));
        platforms_.push_back(ne::Rectf(4849,2000,102,7));
        platforms_.push_back(ne::Rectf(4849,1850,102,5));
        platforms_.push_back(ne::Rectf(4849,1700,102,5));
        platforms_.push_back(ne::Rectf(4849,1550,102,5));
        platforms_.push_back(ne::Rectf(4849,1400,102,5));
        platforms_.push_back(ne::Rectf(4849,1250,102,5));
        platforms_.push_back(ne::Rectf(4449,2251,302,5));
        platforms_.push_back(ne::Rectf(2799,1600,102,5));
        platforms_.push_back(ne::Rectf(2799,1850,102,5));
        platforms_.push_back(ne::Rectf(2799,2050,101,5));
        platforms_.push_back(ne::Rectf(2900,2150,101,5));
        platforms_.push_back(ne::Rectf(2749,2451,152,5));
        platforms_.push_back(ne::Rectf(3149,1400,51,4));
        platforms_.push_back(ne::Rectf(3350,1300,150,5));
        platforms_.push_back(ne::Rectf(3600,1200,51,5));
        platforms_.push_back(ne::Rectf(3049,1150,201,5));
        platforms_.push_back(ne::Rectf(3999,1001,152,5));
        platforms_.push_back(ne::Rectf(3999,851,152,5));
        platforms_.push_back(ne::Rectf(3999,701,152,5));
        platforms_.push_back(ne::Rectf(3999,551,152,5));
    }

    void load()
    {
        tile_map_.loadFromFile("data/level1.tmx", "data/finish.png");
        setCollisions();

        // Collisions to prevent falling off the map
        collisions_.push_back(ne::Rectf(-200, 0, 200, tile_map_.getPixelSize().y));
        collisions_.push_back(ne::Rectf(0, -200, tile_map_.getPixelSize().x, 200));
        collisions_.push_back(ne::Rectf(0, tile_map_.getPixelSize().y, tile_map_.getPixelSize().x, 200));
        collisions_.push_back(ne::Rectf(tile_map_.getPixelSize().x, 0, 200, tile_map_.getPixelSize().y));

        player_->setCollisions(&collisions_);
        player_->setPlatforms(&platforms_);
        player_->setPosition(ne::Vector2f(110, 2394));
        player_->getDrone()->setPosition(player_->getCentre());
        player_->getDrone()->getLight()->setPosition(player_->getCentre(), true);

        player_->setBatteryThres(1.0);

        camera_->setLevelSize(tile_map_.getPixelSize());
        camera_->setPosition(player_->getCentre());
        
        enemies_.push_back(new Merman(ne::Vector2f(4000, 1494)));
        enemies_.push_back(new Merman(ne::Vector2f(2205, 2804)));

        enemies_.push_back(new Viper(ne::Vector2f(1266, 2375)));
        enemies_.push_back(new Viper(ne::Vector2f(2448, 2388)));
        enemies_.push_back(new Viper(ne::Vector2f(4454, 1076)));
        enemies_.push_back(new Viper(ne::Vector2f(4162, 1487)));
        enemies_.push_back(new Viper(ne::Vector2f(3065, 1337)));

        // Replace with blobs if have time
        enemies_.push_back(new Viper(ne::Vector2f(3344, 2391)));

        enemies_.push_back(new Grate(ne::Vector2f(800, 2394)));
        enemies_.push_back(new Grate(ne::Vector2f(621, 2044)));
        enemies_.push_back(new Grate(ne::Vector2f(3000, 1094)));
        enemies_.push_back(new Grate(ne::Vector2f(1200, 1245)));
        enemies_.push_back(new Grate(ne::Vector2f(3817, 2044))); 
        enemies_.push_back(new Grate(ne::Vector2f(3796, 1094))); 
        for (unsigned int i = 0; i < enemies_.size(); i++)
        {
            enemies_[i]->setCollisions(&collisions_);
            enemies_[i]->setPlatforms(&platforms_);
        }

        // Spawn Enemies!
        
        // Merman which spawns behind you
        interactables_.push_back(new Spawner(SPAWNER, 1107, 1152, 98, 150, MERMAN_NOALERT, ne::Vector2f(649, 1250)));
        // First drop Viper
        interactables_.push_back(new Spawner(SPAWNER, 1827, 1001, 100, 153, VIPER_NOALERT, ne::Vector2f(2116, 743)));
        // Second drop Viper
        interactables_.push_back(new Spawner(SPAWNER, 4799, 2049, 151, 202, VIPER_NOALERT, ne::Vector2f(4877, 1177)));

        // Upgrades!
    
        interactables_.push_back(new Upgrade(UPGRADE, 4856, 2186, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 3855, 485, 32, 32, DAMAGE));
        interactables_.push_back(new Upgrade(UPGRADE, 436, 2845, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 2775, 1032, 32, 32, HEALTH));

        // Weapons!
        interactables_.push_back(new Weapon(KNIFE, 972, 1691, 32, 32));

        // Ammo!
        interactables_.push_back(new Ammo(AMMO, 3308, 2607, 32, 32));
        interactables_.push_back(new Ammo(AMMO, 4259, 2157, 32, 32));
        interactables_.push_back(new Ammo(AMMO, 859, 2910, 32, 32));
        interactables_.push_back(new Upgrade(UPGRADE, 3010, 2600, 32, 32, BATTERY));
        interactables_.push_back(new Upgrade(UPGRADE, 3010, 2600, 32, 32, BATTERY));


        // Dialog!
        interactables_.push_back(new Location(LOCATION, "Try not to hurt yourself.", 972, 1691, 32, 32));
        interactables_.push_back(new Location(LOCATION, "Be careful; I detect enemies.", 313, 1825, 84, 27));
        interactables_.push_back(new Location(LOCATION, "Watch out! Behind you!", 1107, 1152, 98, 150));
        interactables_.push_back(new Location(LOCATION, "The fish here have gone mad.", 501, 2315, 49, 35)); 
        interactables_.push_back(new Location(LOCATION, "I'm your light, don't let my power reserves run out!", 549, 1297, 53, 104));  
        interactables_.push_back(new Location(LOCATION, "You have to crouch to kill the Viper.", 1827, 1001, 100, 153));
        interactables_.push_back(new Location(LOCATION, "You will need a weapon to break this grate.", 674, 2374, 48, 77));          
        interactables_.push_back(new Location(LOCATION, "I wouldn't go down there...", 3347, 1201, 154, 98));
        interactables_.push_back(new Location(LOCATION, "There may be weapon upgrades around here.", 3948, 1052, 51, 99));
        interactables_.push_back(new Location(LOCATION, "This used to be a research station.", 3051, 1315, 150, 83));
        interactables_.push_back(new Location(LOCATION, "The salad bar was fantastic.", 2800, 1796, 99, 49));
        interactables_.push_back(new Location(LOCATION, "What's over here?", 4376, 2783, 59, 63));
        
        // Level!
        interactables_.push_back(new Transition(LEVEL2, 4449, 3250, 302, 400));
    }
  
 private:
};

