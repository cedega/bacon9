#pragma once
#include "Player.h"
#define X_JUMP_DISTANCE 125

class Enemy : public ne::Entity
{
 public:
    Enemy()
    {
        alive_ = true;
        respects_alert_ = true;
        health_ = 0;
        damage_ = 0;
        knockback_id_ = -1;
        aggro_timer_ = 0.0f;
        knifed_ = false;
        critical_distance_ = 250;
    }

    virtual ~Enemy() {}

    bool takeDamage(int damage, bool knife)
    {
        // Prevent a single knife attack from applying more than once
        if (knifed_)
            return false;
        
        if (health_ > 1) // This will make it so that this sound will not try to get played when grate is attacked since it only has 1hp.
            hit_sound_.play();
        
        health_ -= damage;

        if (health_ <= 0)
        {
            killed_sound_.play();
            health_ = 0;
            alive_ = false;
        } 

        // Prevent a single knife attack from applying more than once
        if (knife)
            knifed_ = true;

        return true;
    }

    int getDamage() 
    {
        return damage_;
    }

    bool isAlive() const
    {
        return alive_;
    }

    void resetKnife()
    {
        knifed_ = false;
    }

    virtual void transitionAnimations() 
    {
        if (!alive_)
        {
            loadAnimation("death");
        }
        else if (isAnimating("death"))
        {
            // Prevent state transition
        }
        else if (!isMoving() && !isAnimating("idle"))
        { 
            loadAnimation("idle");
        }
        else if (!isAnimating("walk") && isMoving())
        {
            loadAnimation("walk");
        }
    }

    void update(const float dt)
    {
        ne::Entity::update(dt);
    }

    virtual void AI(const Player *player, const float dt) 
    {
        if (!alive_)
        {
            ne::Entity::input(ne::Vector2f(0, 0), false, false);
            return;
        }

        // visibility testing
        bool visible = true;
        ne::Vector2f difference;
        if (getCollisions() != NULL)
        {
            for (size_t i = 0; i < getCollisions()->size(); i++)
            {
                difference = getPosition() - player->getPosition();
                float distance = difference.length();
                float d;
                if (ne::intersects(player->getPosition(), getPosition() - player->getPosition(), getCollisions()->at(i), &d) )
                {
                    if (d <= distance)
                    {
                        visible = false;
                        break;
                    }
                }
            }
        }

        ne::Vector2f dist_from_player = player->getPosition() - getPosition();
        // within range of player
        if (( (dist_from_player.ne::Vector2f::length() < alert_distance_ && respects_alert_) || !respects_alert_) &&
                (visible || dist_from_player.ne::Vector2f::length() < critical_distance_ || !respects_alert_))
        {
            aggro_timer_ += dt / 2;
            setSpeed(std::min(getSpeed() + aggro_timer_, max_speed_));
            ne::Vector2f direction;
            if (ne::absval(dist_from_player.x) > 50)
            {
                direction.x = dist_from_player.x;
                prev_dir_ = direction.x;
            }
            else
            {
                direction.x = prev_dir_;
            }
            if (ne::absval(direction.x) < 1)
            {
                direction.x = 0;
            }
            // player above enemy
            if (player->getPosition().y < getPosition().y &&
                    // x position where jumping starts
                    ne::absval(dist_from_player.x) < X_JUMP_DISTANCE &&
                    // y position difference not less than some arbitrary small value to prevent constant jumping
                    ne::absval(dist_from_player.y) > 75 )
            {
                    ne::Entity::input(direction, true, true);
            }
            // player below enemy
            else if (player->getPosition().y > getPosition().y &&
                    ne::absval(dist_from_player.y) > 10)
            {
                fallThroughPlatform(true);
                ne::Entity::input(direction, false, true);
            }
            else if (player->getPosition().y < getPosition().y)
            {
                fallThroughPlatform(false);
                ne::Entity::input(direction, false, true);
            }
            else
            {
                ne::Entity::input(direction, false, true);
            } 
        }
        // outside of alert distance
        else
        {
            ne::Entity::input(ne::Vector2f(0, 0), false, false);
            aggro_timer_ = 0;
        }
    }


    virtual void load() = 0;
    virtual void knockBack(int direction) = 0;

 protected:
    bool alive_;
    int health_;
    int damage_;
    ne::EntityData data_;
    float aggro_timer_;
    float max_speed_;
    float alert_distance_;
    int knockback_id_;
    bool knifed_;
    bool respects_alert_;
    float critical_distance_;
    float prev_dir_;
    ne::Sound killed_sound_;
    ne::Sound hit_sound_;

 private:
};

