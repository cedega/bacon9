#include "Engine.h"

Engine::Engine()
{
    initWindow("Bacon Game Jam 9", 800, 600, false);
    title_screen_.load();
    map_.load();
}

void Engine::events(const SDL_Event &event)
{
    if (event.type == SDL_QUIT)
    {
        ne::Engine::kill();
    }
}

void Engine::logic(const float dt)
{ 
    ne::Keys keyboard = ne::Input::getKeyboardState();
    if (keyboard[SDL_SCANCODE_ESCAPE])
        ne::Engine::kill();
    
    if (title_screen_.getStatus())
        map_.update(dt);
    else
        title_screen_.logic();
}

void Engine::render(ne::Renderer *renderer)
{
    if (title_screen_.getStatus())
        map_.draw(renderer);
    else
        title_screen_.draw(renderer);
}

