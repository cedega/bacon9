#include "Merman.h"

ne::Image Merman::image_;

Merman::Merman(ne::Vector2f pos)
{
    load();
    health_ = 10;
    damage_ = 25.0f;
    max_speed_ = 250.0f;
    alert_distance_ = 400;
    setSpeed(25.0f);
    setJump(900.0f);
    setPosition(pos);
    respects_alert_ = true;
}

Merman::Merman(ne::Vector2f pos, bool alert)
{
    load();
    health_ = 10;
    damage_ = 25.0f;
    max_speed_ = 250.0f;
    alert_distance_ = 400;
    setSpeed(25.0f);
    setJump(900.0f);
    setPosition(pos);
    respects_alert_ = true;
    respects_alert_ = alert;
} 

void Merman::init()
{
    image_.loadFileImage("data/oarmanspritesheet.png");
}

void Merman::load()
{
    //  load images and shit here
    setHitbox(ne::Vector2f(20, 65));

    // Loads the spritesheet with "movement" animations
    data_.setTexture(&image_); 
    data_.setSpriteSheet(150, 100, 2);
    data_.setBaseDisplacement(-91, -34, 20);
                                            
    // ------------------------------------------------
    // Initialize "movement" animations
    // ------------------------------------------------
    data_.newAnimation("idle", 1, 5, 100);
        data_.setLoop(false);
    data_.newAnimation("walk", 6, 11, 100);
    data_.newAnimation("death", 12, 22, 100);
        data_.setLoop(false);

    // Bind both pieces of data to the image handling portion of ne::Entity (ne::EntityImage)
    ne::EntityImage::attach(&data_);

    // Initalially load the idle animation, and set the speed and jump height of the entity.
    loadAnimation("idle");
    
    // Load Sounds
    killed_sound_.loadFileWAV("data/new_death.wav");
    hit_sound_.loadFileWAV("data/enemy_hit.wav");
}

void Merman::knockBack(int direction)
{
    if (!forceExistsX(knockback_id_))
    {
        knockback_id_ = addForceX(200 * -direction, 0, 150); 
        setSpeed(0); 
    }
}

