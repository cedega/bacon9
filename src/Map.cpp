#include "Map.h"

Map::Map()
{
    Level::init(&player_, &camera_);
    game_over_ = false;
    last_health_ = 100;
    game_over_timer_ = 0.0f;
    game_over_alpha_ = 0.0f;
    game_over_text_visible_ = false;
    music_time_ = 0.0f;
    music_on_ = false;
    drone_on_ = false;
}

Map::~Map()
{
    intro_.stop();
    loop_.stop();

    delete level_;
}

void Map::load()
{
    camera_.setScreenSize(ne::Vector2i(800, 600));
    camera_.setLevelSize(ne::Vector2i(3000, 1200));
    camera_.setLerpSpeed(16.0f);

    game_over_image_.loadFileImage("data/gameover.png");
    game_over_image_.setAlphaMod(0);
    credits_.loadFileImage("data/credits.png");
    credits_.setAlphaMod(0);
    end_timer_ = 0;

    game_over_text_.loadFileFont("data/font.ttf", 20);
    game_over_text_.setString("Press the A button / F key to Restart");
    game_over_text_.setPosition(ne::Vector2f(400 - game_over_text_.getW()/2.0f, 500 - game_over_text_.getH()/2.0f));

    font_.loadFileFont("data/font.ttf", 12);

    Viper::init();
    Merman::init();
    Grate::init();
    Upgrade::init();
    Weapon::init();
    Ammo::init();
    Blob::init();
    Interactable::init(&font_);
    Boss::init();

    player_.load();
    
    ui_.load();
    
    level_ = new Level1();
    level_->load();
    
    intro_.loadFileMusic("data/Intro.ogg");
    loop_.loadFileMusic("data/Loop.ogg");
    intro_.setVolume(75);
    loop_.setVolume(75);
}

void Map::draw(ne::Renderer *renderer)
{
    renderer->setCamera(camera_);
    level_->draw(renderer);
    
    renderer->setCamera(ne::Vector2f(0.0,0.0));
    ui_.draw(renderer);
    player_.drawUI(renderer);


    if (game_over_)
    {
        renderer->draw(game_over_image_);
        if (game_over_text_visible_)
            renderer->draw(game_over_text_);
    }

    if (level_->getEnd())
    {
        renderer->setCamera(ne::Vector2f(0.0,0.0));
        credits_.setAlphaMod(end_timer_);
        renderer->draw(credits_);
    }
}

void Map::update(const float dt)
{
    if (!ne::Music::playing())
        intro_.play(1);

    music_time_ += dt;
    if (!music_on_ && music_time_ > 13.68f)
    {
        loop_.play(-1);
        music_on_ = true;
    }
    
    if (player_.getHealth() != last_health_)
    {
        ui_.setHealth(player_.getHealth()/((player_.getMaxHealth()*1.0)));
        last_health_ = player_.getHealth();
    }
    
    player_.drainBattery(dt);
    ui_.updateBattery(player_.getBattery(), player_.getMaxBattery());
    
    bool gameover;
    int next_level = level_->update(dt, &gameover);
    camera_.moveTo(player_.getCentre(), dt);

    game_over_ = gameover;

    if (game_over_)
    {

        bool reset = false;
        game_over_timer_ += dt;

        game_over_alpha_ += dt / 3.0f;
        if (game_over_alpha_ > 1.0f)
            game_over_alpha_ = 1.0f;

        game_over_image_.setAlphaMod(255*game_over_alpha_);

        if (game_over_timer_ > 5.0f)
            game_over_text_visible_ = true;

        if (game_over_text_visible_)
        {
            std::vector<SDL_GameController*> *controllers = ne::Input::getControllers();

            if (controllers->empty())
            {
                ne::Keys keyboard = ne::Input::getKeyboardState();
                reset = keyboard[SDL_SCANCODE_F];

                game_over_text_.setString("Press the F key to Restart");
                game_over_text_.setPosition(ne::Vector2f(400 - game_over_text_.getW()/2.0f, 500 - game_over_text_.getH()/2.0f));
            }
            else
            {
                SDL_GameController *pad = controllers->at(0);
                reset = ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_A);

                game_over_text_.setString("Press the A button to Restart");
                game_over_text_.setPosition(ne::Vector2f(400 - game_over_text_.getW()/2.0f, 500 - game_over_text_.getH()/2.0f));
            }
        }

        if (reset)
        {
            player_.reset();
            delete level_;
            level_ = new Level1();
            level_->load();

            // Reset Map
            game_over_timer_ = 0.0f;
            game_over_ = false;
            game_over_alpha_ = 0.0f;
            game_over_image_.setAlphaMod(0);
            game_over_text_visible_ = false;
        }

    }

    if (level_->getEnd() )
    {
        end_timer_ +=  dt + (dt * 50);
    }

    if (next_level != -1)
    {
        delete level_;

        switch (next_level)
        {
        case 2: level_ = new Level2(); break;
        case 3: level_ = new Level3(); break;
        //case 3: level_ = new 
        default: break;
        }

        level_->load();
    }
}

