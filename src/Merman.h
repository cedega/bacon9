#pragma once
#include "Enemy.h"

class Merman : public Enemy
{
 public:
    Merman(ne::Vector2f);
    Merman(ne::Vector2f, bool);
    static void init();
    void load();
    void knockBack(int direction);

 private:
    static ne::Image image_;
};

