#pragma once
#include "Interactable.h"
#include "Merman.h"
#include "Viper.h"

enum SPAWN_TYPE
{
    VIPER,
    MERMAN,
    VIPER_NOALERT,
    MERMAN_NOALERT   
};

class Spawner : public Interactable
{
    public:
        Spawner(ID id, float x, float y, float width, float height, SPAWN_TYPE, ne::Vector2f pos);
        Enemy* getEnemy();
        ne::Vector2f getPos();
        void load() {}
        void onPickup(Player*);
    private:
        SPAWN_TYPE type_;
};

