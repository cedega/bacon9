#pragma once
#include <Neru/Renderer.h>
#include <Neru/Input.h>
#include <Neru/Sound.h>
#include <Neru/Music.h>
#include "Drone.h"
#include "Bullet.h"

enum ID
{
    KNIFE,
    HARPOON,
    AMMO,
    LOCATION,
    SPAWNER,
    UPGRADE,
    LEVEL2,
    LEVEL3
}; 

enum UPGRADE_TYPE
{
    HEALTH,
    BATTERY,
    DAMAGE
};

struct WeaponData
{
    ne::Image image;
    int damage;
};

struct Inventory
{
    bool knife;
    bool harpoon;
    int ammo;
};

class Player : public ne::Entity
{
 public:
    Player();
    ~Player();
    void load();
    void reset();
    void input();
    void logic(const float dt);
    void update(const float dt);
    void draw(ne::Renderer *renderer);
    void drawLight(ne::Renderer *renderer);
    void drawUI(ne::Renderer *renderer);
    void transitionAnimations(); 
    void giveWeapon(ID);
    void takeDamage(int, int direction);
    void drainBattery(const float dt);
    void getUpgrade(int);

    bool isAlive() const;
    int getHealth() const;
    bool knifeDone() const;
    Drone* getDrone();
    std::vector<Bullet*>* getBullets();
    int getHealth();
    int getMaxHealth();
    int getBattery();
    int getMaxBattery();
    int getDamage();
    bool getBatteryAlive();
    void setBatteryThres(float thres);

 private:
    bool alive_;
    bool battery_alive_;
    int health_;
    int max_health_;
    int battery_;
    int max_battery_;
    int knife_damage_;
    int harpoon_damage_;
    float battery_thres_;
    float battery_accum_;
    bool attacking_;
    bool crouching_;
    bool shoot_;
    float hurt_timer_;
    ne::EntityData data_;
    ne::EntityData harpoon_data_;
    Drone drone_;

    Inventory inventory_;
    int current_weapon_;
    WeaponData weapons_[2];

    bool knife_done_;
    int hit_force_id_;

    std::vector<Bullet*> bullets_;
    bool firing_;
    
    ne::Sound knife_sound_;
    ne::Sound harpoon_sound_;
    ne::Sound death_sound_;
    ne::Sound hurt_sound_;

    ne::Image harpoon_ammo_;
    ne::Text harpoon_ammo_text_;
};

