#include "Transition.h"

Transition::Transition(ID id, float x, float y, float width, float height)
{ 
    id_ = id;
    hitbox_ = ne::Rectf(x, y, width, height);
    active_ = true;
} 

void Transition::onPickup(Player *player)
{
    if (active_)
    {
        active_ = false;
    }
}

ne::Rectf Transition::getHitbox()
{
    return hitbox_;
}

