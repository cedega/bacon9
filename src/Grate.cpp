#include "Grate.h"

ne::Image Grate::image_;

Grate::Grate(ne::Vector2f pos)
{
    load();
    health_ = 1;
    damage_ = 0;
    max_speed_ = 0.1f;
    alert_distance_ = 0;
    setSpeed(0.0f);
    setJump(0.0f);
    setPosition(pos);
}

void Grate::init()
{
    image_.loadFileImage("data/gratespritesheet.png");
}

void Grate::load()
{
    setHitbox(ne::Vector2f(24, 98));

    data_.setTexture(&image_);
    data_.setSpriteSheet(128, 128, 2);
    data_.setBaseDisplacement(-10, -30, 24);

    data_.newAnimation("idle", 1, 1, 50);
        data_.setLoop(true);

    data_.newAnimation("death", 2, 8, 50);
        data_.setLoop(false);
    
    ne::EntityImage::attach(&data_);
    loadAnimation("idle");
    
    // Load sounds
    killed_sound_.loadFileWAV("data/grate_crash.wav");
}

void Grate::knockBack(int direction)
{
    // do nothing comeon knocking back a grate what the fuck
}

