#include "Level.h"

Player *Level::player_ = NULL;
ne::Camera *Level::camera_ = NULL;

Level::~Level()
{
    for (unsigned int i = 0; i < enemies_.size(); i++)
        delete enemies_[i];

    for (unsigned int i = 0; i < interactables_.size(); i++)
        delete interactables_[i];
}

void Level::init(Player *player, ne::Camera *camera)
{
    player_ = player;
    camera_ = camera;
}

void Level::draw(ne::Renderer *renderer)
{
    renderer->draw(tile_map_, 0);

    for (unsigned int i = 0; i < interactables_.size(); i++)
        interactables_[i]->draw(renderer);

    player_->draw(renderer);
    // renderer->draw(player_->getHitbox(), ne::toSDLColor(0, 0, 0));

    for (unsigned int i = 0; i < enemies_.size(); i++)
    {
        renderer->draw(*enemies_[i]);
        // renderer->draw(enemies_[i]->getHitbox(), ne::toSDLColor(0,0,0));
    }

    /*
    for (unsigned int i = 0; i < interactables_.size(); i++)
    {
        renderer->draw(*interactables_[i]);
    }
    */

    // Debug
    // for (unsigned int i = 0; i < collisions_.size(); i++)
    //     renderer->draw(collisions_[i], ne::toSDLColor(0, 0, 0));
    // for (unsigned int i = 0; i < platforms_.size(); i++)
    //     renderer->draw(platforms_[i], ne::toSDLColor(0, 0, 0));

    player_->drawLight(renderer);
}

int Level::update(const float dt, bool *gameover)
{
    player_->input();
    player_->logic(dt);
    player_->update(dt);

    for (unsigned int i = 0; i < enemies_.size(); i++)
    {
        if (player_->knifeDone())
            enemies_[i]->resetKnife();

        enemies_[i]->logic(dt);
        enemies_[i]->AI(player_, dt);
        enemies_[i]->update(dt); 

        if (enemies_[i]->isAlive())
        {
            // Enemies collide with Player to damage him
            if (player_->isAlive() && ( ne::intersects(player_->getHitbox(), enemies_[i]->getHitbox()) || (enemies_[i]->isActiveFrame() && ne::intersects(player_->getHitbox(), enemies_[i]->getActiveRegion()))) && enemies_[i]->getDamage() > 0)
            {
                int direction = enemies_[i]->getCentre().x - player_->getCentre().x > 0.0f ? 1 : -1;
                player_->takeDamage(enemies_[i]->getDamage(), direction);
                // enemies_[i]->knockBack(enemies_[i]->getDirection());
            }
            else if (enemies_[i]->getDamage() <= 0)
            {
                ne::Rectf p = player_->getHitbox();
                const ne::Rectf q = enemies_[i]->getHitbox();
                player_->ne::EntityLogic::collide(&p, &q);
                player_->setPosition(ne::Vector2f(p.x, p.y));
            }

            if (enemies_[i]->isAlive() && player_->isActiveFrame() && ne::intersects(player_->getActiveRegion(), enemies_[i]->getHitbox()))
            {
                if (enemies_[i]->takeDamage(player_->getDamage(), true))
                    enemies_[i]->knockBack(enemies_[i]->getDirection());
            }

            // Collide bullets on Enemy
            for (int j = 0; j < static_cast<int>(player_->getBullets()->size()); j++)
            {
                Bullet *bullet = player_->getBullets()->at(j);

                if (ne::intersects(bullet->getHitbox(), enemies_[i]->getHitbox()))
                {
                    if (enemies_[i]->takeDamage(bullet->getDamage(), false))
                        enemies_[i]->knockBack(enemies_[i]->getDirection());

                    delete bullet;
                    player_->getBullets()->erase(player_->getBullets()->begin() + j);
                    j--;
                }
            }
            // enemy on enemy collision
            for (unsigned int j = 0; j < enemies_.size(); j++)
            {
                if ( j == i || !enemies_[j]->isAlive())
                    continue;
                ne::Rectf p = enemies_[i]->getHitbox();
                const ne::Rectf q = enemies_[j]->getHitbox();
                enemies_[i]->ne::EntityLogic::collide(&p, &q);
                enemies_[i]->setPosition(ne::Vector2f(p.x, p.y)); 
            }
        }
    }

    // Collide bullets on Collision objects
    if (!player_->getBullets()->empty())
    for (unsigned int i = 0; i < collisions_.size(); i++)
    {
        for (int j = 0; j < static_cast<int>(player_->getBullets()->size()); j++)
        {
            Bullet *bullet = player_->getBullets()->at(j);

            if (ne::intersects(bullet->getHitbox(), collisions_[i]))
            {
                delete bullet;
                player_->getBullets()->erase(player_->getBullets()->begin() + j);
                j--;
            }
        }
    }

    int next_level = -1;

    for (unsigned int i = 0; i < interactables_.size(); i++)
    {
        if (ne::intersects(player_->getHitbox(), interactables_[i]->getHitbox()) && interactables_[i]->getActive())
        {
            if (interactables_[i]->getID() == SPAWNER)
            {
                Enemy *enemy = interactables_[i]->getEnemy();
                enemies_.push_back(enemy);
                enemies_.back()->setCollisions(&collisions_);
                enemies_.back()->setPlatforms(&platforms_);
                enemies_.back()->setPosition(interactables_[i]->getPos());

                enemies_.back()->logic(0);
                enemies_.back()->AI(player_, 0);
                enemies_.back()->update(0);
            }
            else if (interactables_[i]->getID() == LEVEL2)
            {
                next_level = 2;      
            }
            else if (interactables_[i]->getID() == LEVEL3)
            {
                next_level = 3;
            }
            interactables_[i]->onPickup(player_);
        }
    }

    *gameover = !player_->isAlive();

    return next_level;
}

