#include "Location.h"

Location::Location(ID id, const std::string &text, float x, float y, float width, float height)
{ 
    id_ = id;
    text_.loadFileFont("data/font.ttf");
    text_.setString(text);
    hitbox_ = ne::Rectf(x, y, width, height);
    active_ = true;
} 

void Location::onPickup(Player *player)
{
    if (active_)
    {
        player->getDrone()->setText(text_.getString());
        active_ = false;
    }
}

ne::Rectf Location::getHitbox()
{
    return hitbox_;
}

