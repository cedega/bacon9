#pragma once
#include <Neru/Camera.h>
#include <Neru/Text.h>
#include <Neru/Music.h>
#include <Neru/Sound.h>
#include "Level1.h"
#include "Level2.h"
#include "Level3.h"
#include "Player.h"
#include "UI.h"
#include "Upgrade.h"
#include "Weapon.h"
#include "Ammo.h"

class Map
{
 public:
    Map();
    ~Map();
    void load();

    void draw(ne::Renderer *renderer);
    void update(const float dt);

 private:
    ne::Camera camera_;
    Player player_;
    Level *level_;
    UI ui_;
    int last_health_;
    float game_over_timer_;
    float game_over_alpha_;
    bool game_over_;
    ne::Image game_over_image_;
    ne::Text game_over_text_;
    ne::Image credits_;
    bool game_over_text_visible_;

    ne::Music intro_;
    ne::Music loop_;
    float music_time_;
    bool music_on_;
    ne::Text font_;
    bool drone_on_;
    float end_timer_;
};

