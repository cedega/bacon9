#pragma once
#include "Level.h"

class Level3 : public Level
{
 public:
    Level3()
    {
        end_flag_ = false; 
    }

    int update(const float dt, bool *gameover);
    void draw(ne::Renderer *renderer);

    void setCollisions()
    {
        collisions_.push_back(ne::Rectf(1,1,398,248));
        collisions_.push_back(ne::Rectf(2,94,47,906));
        collisions_.push_back(ne::Rectf(9,951,961,44));
        collisions_.push_back(ne::Rectf(751,800,245,207));
        collisions_.push_back(ne::Rectf(951,18,41,817));
        collisions_.push_back(ne::Rectf(601,0,379,250));
        collisions_.push_back(ne::Rectf(21,210,181,90));
        collisions_.push_back(ne::Rectf(13,288,114,87));
        collisions_.push_back(ne::Rectf(924,228,46,98));
        collisions_.push_back(ne::Rectf(886,220,60,68));

        platforms_.push_back(ne::Rectf(399,201,202,6));
        platforms_.push_back(ne::Rectf(850,650,100,7));
        platforms_.push_back(ne::Rectf(49,800,702,7));
    }

    void load()
    {
        tile_map_.loadFromFile("data/FinalLevel.tmx", "data/finish.png");
        setCollisions();

        // Collisions to prevent falling off the map
        collisions_.push_back(ne::Rectf(-200, 0, 200, tile_map_.getPixelSize().y));
        collisions_.push_back(ne::Rectf(0, tile_map_.getPixelSize().y, tile_map_.getPixelSize().x, 200));
        collisions_.push_back(ne::Rectf(tile_map_.getPixelSize().x, 0, 200, tile_map_.getPixelSize().y));
        collisions_.push_back(ne::Rectf(4, -800, 396, 800 + 249));
        collisions_.push_back(ne::Rectf(600, -800, 400, 800 + 250));

        player_->setCollisions(&collisions_);
        player_->setPlatforms(&platforms_);
        player_->setPosition(ne::Vector2f(500, -200));
        player_->getDrone()->setPosition(player_->getCentre());
        player_->getDrone()->getLight()->setPosition(player_->getCentre(), true);
        player_->getDrone()->input(90.0f);

        player_->setBatteryThres(2.0);

        camera_->setLevelSize(tile_map_.getPixelSize());
        camera_->setPosition(player_->getCentre());

        // Default Enemies
        
        enemies_.push_back(new Boss(ne::Vector2f(500, 700)));

        for (unsigned int i = 0; i < enemies_.size(); i++)
        {
            enemies_[i]->setCollisions(&collisions_);
            enemies_[i]->setPlatforms(&platforms_);
        }

        interactables_.push_back(new Upgrade(UPGRADE, 483, 147, 32, 32, HEALTH));
    }

    bool getEnd() { return end_flag_; }
  
 private:
};

