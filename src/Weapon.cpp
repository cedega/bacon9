#include "Weapon.h"

ne::Image Weapon::knife_image_;
ne::Image Weapon::harpoon_image_;

Weapon::Weapon(ID id, float x, float y, float width, float height)
{
    id_ = id;
    hitbox_ = ne::Rectf(x, y, width, height);
    active_ = true;

    load();
}

void Weapon::init()
{
    knife_image_.loadFileImage("data/knife.png");
    harpoon_image_.loadFileImage("data/gun.png");
}

void Weapon::load()
{
    switch (id_)
    {
    case KNIFE: image_.setTexture(&knife_image_); info_.setString("KNIFE"); break;
    case HARPOON: image_.setTexture(&harpoon_image_); info_.setString("HARPOON GUN"); break;
    default: break;
    }

    ne::Vector2f centre = ne::Vector2f(hitbox_.x + hitbox_.w / 2.0f, hitbox_.y - 12.0f);
    info_.setPosition(ne::Vector2f(centre.x - info_.getW() / 2.0f, centre.y - info_.getH() / 2.0f));
    image_.setPosition(ne::Vector2f(hitbox_.x, hitbox_.y));
    
    pick_up_sound_.loadFileWAV("data/pick_up_item.wav");
}

void Weapon::onPickup(Player *player)
{
    if (active_)
    {
        pick_up_sound_.play();
        player->giveWeapon(id_);
        active_ = false;
    }
}

void Weapon::draw(ne::Renderer *renderer)
{
    if (active_)
        renderer->draw(image_);
    Interactable::draw(renderer);
}

ne::Rectf Weapon::getHitbox()
{
    return hitbox_;
}


