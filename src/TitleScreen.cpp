#include "TitleScreen.h"

TitleScreen::TitleScreen()
{
    state_ = START;
    status_ = false;
    last_key_ = false;
    play_music_ = false;
}

TitleScreen::~TitleScreen()
{
    song_.stop();
    select_.stop();
}

void TitleScreen::load()
{
    screen_.loadFileImage("data/titlescreen.png");
    tutorial_page_.loadFileImage("data/tutorialscreen.png");
    
    start_.loadFileImage("data/play.png");
    start_.setX(85.0);
    start_.setY(286.0);
    
    tutorial_.loadFileImage("data/howto.png");
    tutorial_.setX(83.0);
    tutorial_.setY(411.0);
    
    song_.loadFileMusic("data/TitleScreenMusic.ogg");
    select_.loadFileWAV("data/select.wav");
}

void TitleScreen::logic()
{
    if (!play_music_)
    {
        song_.play();
        play_music_ = true;
    }
    std::vector<SDL_GameController*> *controllers = ne::Input::getControllers();

    if (controllers->empty())
    {
        ne::Keys keyboard = ne::Input::getKeyboardState();
        bool knife = ne::Input::getLeftMouseDown();

        if (keyboard[SDL_SCANCODE_DOWN] && state_ == START)
        {
            select_.play();
            state_ = TUTORIAL;
        }
        if (keyboard[SDL_SCANCODE_UP] && state_ == TUTORIAL)
        {
            select_.play();
            state_ = START;
        }
        if (keyboard[SDL_SCANCODE_RETURN] && state_ == START)
        {
            select_.play();
            state_ = LOAD_GAME;
        }
        if (keyboard[SDL_SCANCODE_RETURN] && state_ == TUTORIAL)
        {
            select_.play();
            state_ = LOAD_TUTORIAL;  
        }            
        if (knife && state_ == LOAD_TUTORIAL && last_key_)
        {
            select_.play();
            state_ = LOAD_GAME;     
        }            
        if (state_ == LOAD_TUTORIAL)
            last_key_ = !knife;
    }
    else
    {
        SDL_GameController *pad = controllers->at(0);
        ne::Vector2f stick = ne::Input::getControllerLeftAxis(pad);
        bool ret = ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_A);
        bool ret2 = ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);

        if (stick.y > 0.4f && state_ == START)
        {
            select_.play();
            state_ = TUTORIAL;
        }
        if (stick.y < -0.4f && state_ == TUTORIAL)
        {
            select_.play();
            state_ = START;
        }
        if (ret && state_ == START)
        {
            select_.play();
            state_ = LOAD_GAME;
        }
        if (ret && state_ == TUTORIAL)
        {
            select_.play();
            state_ = LOAD_TUTORIAL;
        }
        if (ret2 && state_ == LOAD_TUTORIAL && last_key_)
        {
            select_.play();
            state_ = LOAD_GAME;
        }
        if (state_ == LOAD_TUTORIAL)
            last_key_ = !ret;
    }
}

void TitleScreen::draw(ne::Renderer *renderer)
{  
    switch (state_)
    {
        case START:
            renderer->draw(screen_);
            renderer->draw(start_);
            break;
        case TUTORIAL:
            renderer->draw(screen_);
            renderer->draw(tutorial_);
            break;
        case LOAD_TUTORIAL:
            renderer->draw(tutorial_page_);
            break;
        case LOAD_GAME:
            status_ = true;
            song_.stop();
            break;
        default:
            std::cout << "No selection possible" << std::endl;
            break;
    }
}

bool TitleScreen::getStatus()
{
    return status_;
}
