#pragma once
#include <Neru/Camera.h>
#include <Neru/TileMap.h>
#include <stack>
#include "Player.h"
#include "Enemy.h"
#include "Viper.h"
#include "Merman.h"
#include "Grate.h"
#include "Blob.h"
#include "Boss.h"
#include "Interactable.h"
#include "Weapon.h"
#include "Location.h"
#include "Spawner.h"
#include "Upgrade.h"
#include "Transition.h"
#include "Ammo.h"

class Level
{
 public:
    Level() {}
    virtual ~Level(); 
    static void init(Player*, ne::Camera*); 
    virtual void load() = 0;

    virtual void draw(ne::Renderer *renderer);
    virtual bool getEnd() { return false; }

    // Returns the level id of the next transitionable level. If not transitioning, returns -1
    virtual int update(const float dt, bool *gameover);

 protected:
    static Player *player_;
    static ne::Camera *camera_;
    std::vector<Interactable*> interactables_;
    std::vector<Enemy*> enemies_;
    std::vector<ne::Rectf> collisions_;
    std::vector<ne::Rectf> platforms_;
    std::stack<Enemy*> unspawned_;
    int level_id_;
    int next_level_id_;
    ne::TileMap tile_map_;
    bool end_flag_;
};

