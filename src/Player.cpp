#include "Player.h"

Player::Player()
{
    alive_ = true;

    attacking_ = false;
    crouching_ = false;
    shoot_ = false;
    max_health_ = 100;
    health_ = 100;
    max_battery_ = 100;
    battery_ = 100;
    battery_accum_ = 0.0;
    battery_alive_ = true;
    hurt_timer_ = 10.0f;
    knife_damage_ = 2;
    harpoon_damage_ = 10;

    inventory_.knife = false;
    inventory_.harpoon = false;
    inventory_.ammo = 0;

    current_weapon_ = -1;
    knife_done_ = false;
    hit_force_id_ = -1;

    firing_ = false;
}

Player::~Player()
{
    for (unsigned int i = 0; i < bullets_.size(); i++)
        delete bullets_[i];
}

void Player::load()
{
    // Sets the width and height of the Player's hitbox to 20 pixels wide, 95 pixels tall.
    setHitbox(ne::Vector2f(13, 57));

    // Loads the spritesheet with "movement" animations
    data_.loadFileImage("data/ariaspritesheet.png");
    data_.setSpriteSheet(80, 80, 2);
    data_.setBaseDisplacement(-34, -22, 10);

    // ------------------------------------------------
    // Initialize "movement" animations
    // ------------------------------------------------
    data_.newAnimation("idle", 1, 12, 50);
    data_.newAnimation("walk", 13, 24, 50);
    data_.newAnimation("crouch", 25, 31, 50);
        data_.setLoop(false);
    data_.newAnimation("hit", 32, 32, 100);
    data_.newAnimation("death", 33, 49, 100);
        data_.setLoop(false);
    data_.newAnimation("attack", 50, 55, 50);
        data_.displaceAnimation(5, 0);
        data_.setActiveFrames(52, 53); 
        data_.setActiveRegion(52, ne::Rectf(41, 36, 28, 13)); 
        data_.setActiveRegion(53, ne::Rectf(49, 35, 29, 15));
        data_.setLoop(false);
    data_.newAnimation("cattack", 56, 64, 50);
        data_.setActiveFrames(61, 62);
        data_.setActiveRegion(61, ne::Rectf(50, 31, 21, 44));
        data_.setActiveRegion(62, ne::Rectf(39, 20, 30, 55));
        data_.setLoop(false);
    data_.newAnimation("jump", 65, 65, 100);
        data_.setLoop(false);
    data_.newAnimation("fall", 66, 66, 100);
        data_.setLoop(false);

    // ------------------------------------------------
    // HARPOOONN
    // ------------------------------------------------

    harpoon_data_.loadFileImage("data/ariagunspritesheet.png");
    harpoon_data_.setSpriteSheet(100, 100, 2);
    harpoon_data_.setBaseDisplacement(-39, -42, 10);

    harpoon_data_.newAnimation("harpoon", 1, 15, 50);
        harpoon_data_.setLoop(false);
    harpoon_data_.newAnimation("crouch_harpoon", 16, 31, 50);
        harpoon_data_.setLoop(false);
        harpoon_data_.displaceAnimation(-5, 0);

    // Bind both pieces of data to the image handling portion of ne::Entity (ne::EntityImage)
    ne::EntityImage::attach(&data_);
    ne::EntityImage::attach(&harpoon_data_);

    // Initalially load the idle animation, and set the speed and jump height of the entity.
    loadAnimation("idle");
    setSpeed(250.0f);
    setJump(2280.0f);

    drone_.load();
    
    // Load the sounds.
    knife_sound_.loadFileWAV("data/knife.wav");
    death_sound_.loadFileWAV("data/girl_death.wav");
    hurt_sound_.loadFileWAV("data/girl_hurt.wav");
    harpoon_sound_.loadFileWAV("data/harpoon.wav");

    harpoon_ammo_.loadFileImage("data/harpoonammo.png");
    harpoon_ammo_text_.loadFileFont("data/font.ttf", 24);
    harpoon_ammo_text_.setString(ne::toString(inventory_.ammo));

    harpoon_ammo_.setPosition(ne::Vector2f(700, 555));
    harpoon_ammo_text_.setPosition(ne::Vector2f(738, 560));
    
    // Dumb fix for seeing the spritesheet
    update(0.0f);
}

void Player::reset()
{
    alive_ = true;
    battery_alive_ = true;
    max_health_ = 100;
    health_ = 100;
    max_battery_ = 100;
    battery_ = 100;
    battery_thres_ = 0.0f;
    battery_accum_ = 0.0f;
    attacking_ = false;
    crouching_ = false;
    hurt_timer_ = 10.0f;

    knife_damage_ = 2;
    harpoon_damage_ = 10;

    inventory_.knife = false;
    inventory_.harpoon = false;
    inventory_.ammo = 0;
    current_weapon_ = -1;
    knife_done_ = false;
    hit_force_id_ = -1;

    setSpeed(250.0f);
    setJump(2280.0f);
    loadAnimation("idle");
    update(0.0f);

    firing_ = false;

    for (unsigned int i = 0; i < bullets_.size(); i++)
        delete bullets_[i];
    bullets_.clear();

    drone_.reset();
}

void Player::input()
{
    if (!alive_)
    {
        setSpeed(0.0f);
        ne::Entity::input(ne::Vector2f(0.0f, 0.0f), false, false);
        return;
    }

    std::vector<SDL_GameController*> *controllers = ne::Input::getControllers();
    bool ATTACKING = isAnimating("cattack") || isAnimating("attack") || isAnimating("harpoon") || isAnimating("crouch_harpoon");

    if (controllers->empty())
    {
        ne::Keys keyboard = ne::Input::getKeyboardState();

        ne::Vector2f direction;

        // if (keyboard[SDL_SCANCODE_F1])
        //    std::cout << getPosition().x << " " << getPosition().y << std::endl;
        if (keyboard[SDL_SCANCODE_A])
            direction.x -= 1.0f;
        if (keyboard[SDL_SCANCODE_D])
            direction.x += 1.0f;
        if (keyboard[SDL_SCANCODE_S])
            ne::Entity::fallThroughPlatform(true); // Makes the player fall through any platform he's standing on

        attacking_ = inventory_.knife && ne::Input::getLeftMouseDown();
        crouching_ = keyboard[SDL_SCANCODE_LCTRL];
        shoot_ = inventory_.harpoon && ne::Input::getRightMouseDown();

        // 'direction' is the movement direction vector. For a platformer, just the x-axis is sufficient.
        // the second argument is a bool for the jump key
        // the last argument determines whether the sprite image should be horizontally flipped depending on direction.
        //      for platformers, this is required.
        ne::Entity::input(direction, keyboard[SDL_SCANCODE_W] && !crouching_ && !ATTACKING, true);
    }
    else
    {
        SDL_GameController *pad = controllers->at(0);

        ne::Vector2f direction = ne::Input::getControllerLeftAxis(pad);

        if (direction.y > 0.8f)
            ne::Entity::fallThroughPlatform(true);
        direction.y = 0.0f;

        if (ne::absval(direction.x) < 0.2f)
            direction.x = 0.0f;

        attacking_ = inventory_.knife && ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_X);
        crouching_ = ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
        shoot_ = inventory_.harpoon && ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_B);

        // Debug
        // if (ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_Y))
        //    std::cout << ne::toString(getPosition()) << "\n";

        ne::Entity::input(direction, ne::Input::getControllerButton(pad, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER) && !crouching_ && !ATTACKING, true);
    }
}

void Player::logic(const float dt)
{
    if (!forceExistsY(hit_force_id_) && !isAnimating("attack"))
    {
        setSpeed(250.0f);
        setJump(2280.0f);
    }

    ne::Entity::logic(dt);
    ne::Vector2f d;
    float angle = 0.0f;

    std::vector<SDL_GameController*> *controllers = ne::Input::getControllers();
    
    if (alive_)
    {
        if (controllers->empty())
        {
            d = ne::Vector2f(ne::Input::getMousePosition().x, ne::Input::getMousePosition().y);
            angle = RADTODEG*std::atan2(d.y - 300, d.x - 400);
            drone_.input(angle);
        }
        else
        {
            d = ne::Input::getControllerRightAxis(controllers->at(0));
            angle = RADTODEG*std::atan2(d.y, d.x);

            if (d.length() > 0.4f)
                drone_.input(angle);
        }
    }
    else
    {
        d = getCentre() - drone_.getCentre();
        angle = RADTODEG*std::atan2(d.y, d.x);
        drone_.input(angle);
    }

    drone_.AI(this, getBattery(), dt);
}

void Player::update(const float dt)
{
    hurt_timer_ += dt;

    if (hurt_timer_ < 0.3f)
        setColour(ne::toSDLColor(255, 64, 64));
    else
        setColour(ne::toSDLColor(255, 255, 255));

    if (getBattery() <= 0)
    {
        alive_ = false;
        health_ = 0;
    }

    ne::Entity::update(dt);
    
    for (int i = 0; i < static_cast<int>(bullets_.size()); i++)
    {
        bullets_[i]->logic(dt);
        bullets_[i]->update(dt);

        if (!bullets_[i]->isAlive())
        {
            delete bullets_[i];
            bullets_.erase(bullets_.begin() + i);
            i--;
        }
    }

    harpoon_ammo_text_.setString(ne::toString(inventory_.ammo));
}

void Player::draw(ne::Renderer *renderer)
{
    renderer->draw(*this);
    renderer->draw(drone_);
    renderer->draw(drone_.getText());

    // if (isActiveFrame())
    //     renderer->draw(getActiveRegion(), ne::toSDLColor(0, 0, 0));

    for (unsigned int i = 0; i < bullets_.size(); i++)
    {
        renderer->draw(*bullets_[i]);
        // renderer->draw(bullets_[i]->getHitbox(), ne::toSDLColor(0, 0, 0));
    }
}

void Player::drawLight(ne::Renderer *renderer)
{
    drone_.drawLight(renderer);
}

void Player::drawUI(ne::Renderer *renderer)
{
    renderer->draw(harpoon_ammo_);
    renderer->draw(harpoon_ammo_text_);
}

void Player::transitionAnimations()
{
    // Constantly reset knife_done_, but will be asserted when "attack" ends
    knife_done_ = false;

    bool ATTACKING = isAnimating("cattack") || isAnimating("attack") || isAnimating("harpoon") || isAnimating("crouch_harpoon");

    if (!alive_ && !isAnimating("death"))
    {
        loadAnimation("death");
        setSpeed(0.0f);
        setJump(0.0f);
    }
    else if (isAnimating("death"))
    {
        // Nothing -- eat state transitions
    }
    else if (!isJumping() && crouching_ && shoot_ && !ATTACKING && inventory_.ammo > 0)
    {
        loadAnimation("crouch_harpoon");
        inventory_.ammo -= 1;
        setSpeed(0.0f);
    }
    else if (isAnimating("crouch_harpoon"))
    {
        if (animator_.getFrame() == 8 && !firing_)
        {
            ne::Vector2f offset;

            if (getDirection() == 1)
            {
                offset = ne::Vector2f(-4, 1);
            }
            else
            {
                offset = ne::Vector2f(-42, 1);
            }

            bullets_.push_back(new Bullet(getCentre() + offset, ne::Vector2f(getDirection(), 0.0f), 2.0f, harpoon_damage_));
            harpoon_sound_.play();
            firing_ = true;
        }

        if (animationStopped())
        {
            loadAnimation("crouch");
            animator_.setFrame(7);
            firing_ = false;
        }
        setSpeed(0.0f);
    }
    else if (!isJumping() && shoot_ && !isAnimating("harpoon") && !ATTACKING && inventory_.ammo > 0)
    {
        loadAnimation("harpoon");
        inventory_.ammo -= 1;
        setSpeed(0.0f);
    }
    else if (isAnimating("harpoon"))
    {
        if (animator_.getFrame() == 8 && !firing_)
        {
            ne::Vector2f offset;

            if (getDirection() == 1)
            {
                offset = ne::Vector2f(0, -15);
            }
            else
            {
                offset = ne::Vector2f(-35, -15);
            }

            bullets_.push_back(new Bullet(getCentre() + offset, ne::Vector2f(getDirection(), 0.0f), 2.0f, harpoon_damage_));
            harpoon_sound_.play();
            firing_ = true;
        }

        if (animationStopped())
        {
            loadAnimation("idle");
            setSpeed(250.0f);
            firing_ = false;
        }
        setSpeed(0.0f);
    }
    else if (!isJumping() && crouching_ && attacking_ && !ATTACKING)
    {
        knife_sound_.play();
        loadAnimation("cattack");
        setSpeed(0.0f);
    }
    else if (isAnimating("cattack"))
    {
        if (animationStopped())
        {
            knife_done_ = true;
            loadAnimation("crouch");
            animator_.setFrame(7);
        }
        setSpeed(0.0f);
    }
    else if (!isJumping() && crouching_ && !ATTACKING)
    {
        if (!isAnimating("crouch"))
            loadAnimation("crouch");
        setSpeed(0.0f);
    }
    else if (!isJumping() && attacking_ && !ATTACKING)
    {
        knife_sound_.play();
        loadAnimation("attack");
        setSpeed(0.0f);
    }
    else if (isAnimating("attack"))
    {
        if (animationStopped())
        {
            knife_done_ = true;
            loadAnimation("idle");
            setSpeed(250.0f);
        }
    }
    else if (isJumping() && getVelocity().y < 0.0f)
    {
        if (!isAnimating("jump"))
            loadAnimation("jump");
    }
    else if (getVelocity().y > 50.0f)
    {
        if (!isAnimating("fall"))
            loadAnimation("fall");
    }
    else if (!isAnimating("walk") && isMoving())
    {
        loadAnimation("walk");
        setSpeed(250.0f);
    }
    else if (!isMoving())
    {
        loadAnimation("idle");
    }
}

void Player::giveWeapon(ID wep)
{
    switch(wep)
    {
        case KNIFE: inventory_.knife = true; break;
        case HARPOON: inventory_.harpoon = true; break;
        case AMMO: inventory_.ammo += 1; break;
        default: std::cout << "zach fucked up" << std::endl;
    }
}

void Player::takeDamage(int damage, int direction)
{
    if (hurt_timer_ > 1.0f)
    {
        health_ -= damage;

        if (health_ <= 0)
        {
            death_sound_.play();
            health_ = 0;
            alive_ = false;
        }
        else
            hurt_sound_.play();

        hurt_timer_ = 0.0f;

        addForceX(-200*direction, 0, 200);
        hit_force_id_ = applyNoGravityY(addForceY(-200, 0, 150));
        setSpeed(0.0f);
        setJump(0.0f);
    }
}

void Player::drainBattery(const float dt)
{
    if (battery_accum_ >= battery_thres_)
    {
        battery_--;
        battery_accum_ = 0.0;
        if (battery_ <= 0.0 && battery_alive_)
        {
            battery_alive_ = false;
        }
    }
    else
        battery_accum_ += dt;
}

bool Player::isAlive() const
{
    return alive_;
}

int Player::getHealth() const
{
    return health_;
}

bool Player::knifeDone() const
{
    return knife_done_;
}

Drone* Player::getDrone()
{
    return &drone_;
}

std::vector<Bullet*>* Player::getBullets()
{
    return &bullets_;
}

int Player::getHealth()
{
    return health_;
}

int Player::getMaxHealth()
{
    return max_health_;
}

int Player::getBattery()
{
    return battery_;
}

int Player::getMaxBattery()
{
    return max_battery_;
}

void Player::setBatteryThres(float thres)
{
    battery_thres_ = thres;
}

void Player::getUpgrade(int upgrade)
{
    switch (upgrade)
    {
    case HEALTH: max_health_ += 20; health_ = max_health_; break;
    case BATTERY: battery_ = max_battery_ ; drone_.useLight(); break;
    case DAMAGE: knife_damage_ += 2; harpoon_damage_ += 10; break;
    }
}

int Player::getDamage()
{
    return knife_damage_;
}

bool Player::getBatteryAlive()
{
    return battery_alive_;
}
