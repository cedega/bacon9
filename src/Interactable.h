#pragma once
#include "Player.h"
#include "Enemy.h"
#include <Neru/Text.h>
#include <Neru/Sound.h>

class Interactable
{
 public: 
    Interactable()
    {
        info_.setFont(font_->getFont());
    }
    virtual ~Interactable() {}
    static void init(ne::Text *font)
    {
        font_ = font;
    }
    virtual void load() = 0;
    virtual void onPickup(Player*) = 0;
    virtual void draw(ne::Renderer *renderer)
    {
        if (active_)
            renderer->draw(info_);
    }
    virtual ne::Rectf getHitbox() { return hitbox_; }
    int getID() { return id_; }
    bool getActive() { return active_; }
    virtual Enemy* getEnemy() {return en_;}
    ne::Vector2f getPos() { return pos_;}

 protected:
    Player *player_;
    ne::Rectf hitbox_;
    ID id_;
    bool active_;
    ne::Vector2f pos_;
    Enemy *en_;
    static ne::Text *font_;
    ne::Text info_;
    ne::Sound pick_up_sound_;
};
