#include "Bullet.h"

Bullet::Bullet(const ne::Vector2f &position, const ne::Vector2f &direction, float life_time, int damage)
{
    load();
    setPosition(position);
    ne::Entity::input(direction, false, true);
    life_time_ = life_time;
    life_tick_ = 0.0f;
    alive_ = true;
    damage_ = damage;
}

void Bullet::load()
{
    setHitbox(ne::Vector2f(32, 5));
    data_.loadFileImage("data/harpoon.png");
    data_.setSpriteSheet(32, 12, 0);
    data_.setBaseDisplacement(0, -3, 32);

    data_.newAnimation("idle", 1, 1, 100);
        data_.setLoop(false);

    ne::EntityImage::attach(&data_);
    loadAnimation("idle");

    setSpeed(800.0f);
    setGravity(0);
}

void Bullet::update(const float dt)
{
    life_tick_ += dt;

    if (life_tick_ >= life_time_)
    {
        alive_ = false;
    }

    ne::Entity::update(dt);
}

