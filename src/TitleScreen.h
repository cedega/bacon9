#pragma once
#include <Neru/Renderer.h>
#include <Neru/Input.h>
#include <Neru/Music.h>
#include <Neru/Sound.h>

enum States
{
    START,
    TUTORIAL,
    LOAD_TUTORIAL,
    LOAD_GAME,
};

class TitleScreen
{
    public:
        TitleScreen();
        ~TitleScreen();
        
        void load();  
        void logic();
        void draw(ne::Renderer *renderer);
        bool getStatus();
        
    private:
        ne::Image screen_;
        ne::Image start_;
        ne::Image tutorial_;
        ne::Image tutorial_page_;
    
        bool status_;
        bool last_key_;
        bool play_music_;
        
        States state_;
        
        ne::Music song_;
        ne::Sound select_;
};
