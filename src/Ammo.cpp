#include "Ammo.h"

ne::Image Ammo::ammo_image_;

Ammo::Ammo(ID id, float x, float y, float width, float height)
{
    id_ = id;
    hitbox_ = ne::Rectf(x, y, width, height);
    active_ = true;

    load();
}

void Ammo::init()
{
    ammo_image_.loadFileImage("data/ammodrop.png");
}

void Ammo::load()
{
    switch (id_)
    {
    case AMMO: image_.setTexture(&ammo_image_); info_.setString("HARPOON AMMO"); break;
    default: break;
    }

    ne::Vector2f centre = ne::Vector2f(hitbox_.x + hitbox_.w / 2.0f, hitbox_.y - 12.0f);
    info_.setPosition(ne::Vector2f(centre.x - info_.getW() / 2.0f, centre.y - info_.getH() / 2.0f));
    image_.setPosition(ne::Vector2f(hitbox_.x, hitbox_.y));
    
    pick_up_sound_.loadFileWAV("data/pick_up_item.wav");
}

void Ammo::onPickup(Player *player)
{
    if (active_)
    {
        pick_up_sound_.play();
        player->giveWeapon(id_);
        active_ = false;
    }
}

void Ammo::draw(ne::Renderer *renderer)
{
    if (active_)
        renderer->draw(image_);
    Interactable::draw(renderer);
}

ne::Rectf Ammo::getHitbox()
{
    return hitbox_;
}

