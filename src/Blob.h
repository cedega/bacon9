#pragma once
#include "Enemy.h"

class Blob : public Enemy
{
    public:
        Blob(ne::Vector2f);
        Blob(ne::Vector2f, bool);
        static void init();
        void load();
        void AI(const Player*, const float);
        void knockBack(int direction);
        void transitionAnimations();
    private:
        static ne::Image image_;
        bool within_attack_distance_;
};

