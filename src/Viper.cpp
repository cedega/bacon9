#include "Viper.h"
#define X_JUMP_DISTANCE 125

ne::Image Viper::image_;

Viper::Viper(ne::Vector2f pos)
{
    load();
    health_ = 5;
    damage_ = 10;
    max_speed_ = 300.0f;
    alert_distance_ = 400;
    setSpeed(50.0f);
    setJump(900.0f);
    setPosition(pos);
    respects_alert_ = true;
}

Viper::Viper(ne::Vector2f pos, bool alert)
{
    load();
    health_ = 5;
    damage_ = 10;
    max_speed_ = 300.0f;
    alert_distance_ = 400;
    setSpeed(50.0f);
    setJump(900.0f);
    setPosition(pos);
    respects_alert_ = true;
    respects_alert_ = alert;
}

void Viper::init()
{
    image_.loadFileImage("data/viperspritesheet.png");
}

void Viper::load()
{
    //  load images and shit here
    setHitbox(ne::Vector2f(50, 27));

    // Loads the spritesheet with "movement" animations
    data_.setTexture(&image_); 
    data_.setSpriteSheet(128, 128, 2);        
    data_.setBaseDisplacement(-45, -98, 50);

    // ------------------------------------------------
    // Initialize "movement" animations
    // ------------------------------------------------
    data_.newAnimation("idle", 1, 6, 100); 
        data_.setLoop(true);

    data_.newAnimation("walk", 7, 14, 50);

    data_.newAnimation("death", 15, 21, 100);
        data_.setLoop(false);

    // Bind both pieces of data to the image handling portion of ne::Entity (ne::EntityImage)
    ne::EntityImage::attach(&data_);

    // Initalially load the idle animation, and set the speed and jump height of the entity.
    loadAnimation("idle");
    setSpeed(100.0f);
    setJump(900.0f);
    
    // Load Sounds
    killed_sound_.loadFileWAV("data/new_death.wav");
    hit_sound_.loadFileWAV("data/enemy_hit.wav");
}

void Viper::knockBack(int direction)
{
    if (!forceExistsX(knockback_id_))
    {
        knockback_id_ = addForceX(200 * -direction, 0, 150); 
        setSpeed(0);
    }
}

