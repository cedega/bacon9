#include "Enemy.h"

class Grate : public Enemy
{
    public:
        Grate(ne::Vector2f);
        static void init();
        void load();
        void knockBack(int);
    private:
        static ne::Image image_;
};

