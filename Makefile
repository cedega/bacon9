BINARY_NAME=neru

#CONFIG
INCLUDE=../neru/include/
FRAMEWORKS=
CFLAGS=-O2

ifeq ($(OS),Windows_NT)
	MESSAGE="Operating System Detected: Windows"
	COMPILE=../neru/compile/windows/
	LIBS=-lneru -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lminizip -lbz2 -lz -lm -static-libgcc -static-libstdc++ -Wall -mwindows
	BINARY=binaries/windows/$(BINARY_NAME).exe
else ifeq ($(shell uname), Darwin)
	MESSAGE=“Operating System Detected: Mac OSX”
	COMPILE=../neru/compile/mac/
	LIBS=-lneru -framework SDL2 -framework SDL2_image -framework SDL2_mixer -framework SDL2_ttf -framework SDL2_net -lminizip -lz -Wall
	FRAMEWORKS="-F../neru/compile/mac/"
	CFLAGS += -m32
	BINARY=binaries/mac/$(BINARY_NAME)
else
	MESSAGE="Operating System Detected: Linux"
	COMPILE=../neru/compile/linux/
	LIBS=-lneru -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lSDL2_net -lminizip -lz -lm -Wall
	BINARY=binaries/linux/$(BINARY_NAME)
endif

srcs = $(wildcard src/*.cpp)
objs = $(srcs:.cpp=.o)
deps = $(srcs:.cpp=.d)
OBJECTS=src/*.o

all: bin

bin: $(objs)
	@echo $(MESSAGE)
	g++ $(CFLAGS) $(OBJECTS) -I"$(INCLUDE)" -L"$(COMPILE)" $(FRAMEWORKS) $(LIBS) -o $(BINARY)

%.o: %.cpp
	g++ $(CFLAGS) -I"$(INCLUDE)" -MMD -MP -c $< -o $@ -Wall

clean:
	rm -f src/*.o src/*.d

-include $(deps)
